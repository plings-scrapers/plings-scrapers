#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings_sax import Activities
import datetime
import dateutil.parser
import re
import sys
import os

cachedir = "cache/walkit/"
baseurl = "http://walkit.com"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
        url = baseurl + "/walking-events-and-tours/"
        html = urllib2.urlopen(url).read()
        
        page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        for h4 in page1.findAll("h4"):
            url2 = h4.a["href"]
            print url2
            id =  h4.a["href"].replace(baseurl,"")
            if not (id+".html").encode("utf8") in os.listdir(cachedir):
                html = urllib2.urlopen(url2.encode("utf8")).read()
                print id
                f = open(os.path.join(cachedir, id.replace("/","-").encode("utf8")+".html"), "w")
                f.write(html)
                f.close()
                
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "walkit.xml"
    
    for file in os.listdir(cachedir):
        try:
            file = file.decode("utf8")
            if file.endswith(".html"):
                id = file[:-len(".html")]
            
            #print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            print " ".join( page.find(id="eventAddressDetail") .findAll(text=True) )
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            
        except KeyError: print "OH NOES"

    activities.finish()
    #print activities.doc.toprettyxml(indent="    ")
        