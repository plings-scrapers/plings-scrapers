#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings import Activities
import csv
import re
import datetime

def decode_date(date):
    date = date.strip()
    return datetime.datetime.strptime(date, "%d/%m/%y").date()
    
def decode_time(time):
    time = time.strip()
    return datetime.datetime.strptime(time, "%H:%M").time()

spread = csv.reader((open("ISLINGTON-HALFTERM-June2010.csv", "r")))
head1 = spread.next()
head2 = spread.next()

activities = Activities()
activities.outname = "islington_halfterm.xml"

i = 0
for title, desc, date, starts, ends, cost, places, age, \
        venue_name, venue_address, postcode, map, access, public_transport, \
        orgname, contact_title, contact_fname, contact_sname, contact_email, contact_phone, org_website, \
        booking, notes, cat1, cat2, cat3, cat4 in spread:
    activity = activities.create_activity()
    activity.add_field("Name", title)
    activity.add_field("Description", desc)
    try:
        d = decode_date(date)
        activity.add_field("Starts", activity.formatdate(datetime.datetime.combine(d, decode_time(starts))))
        activity.add_field("Ends", activity.formatdate(datetime.datetime.combine(d, decode_time(ends))))
    except: continue
    activity.add_field("Cost", cost)
    try:
        a = age.split(" to ")
        activity.add_field("MinAge", a[0])
        activity.add_field("MaxAge", a[1])
    except: pass
    
    activity.venue.add_field("ProviderVenueID", activity.hash(venue_name))
    activity.venue.add_field("Name", venue_name)
    try:
        r = re.match("\d+", venue_address)
        activity.venue.add_field("BuildingNameNo", r.group())
    except: pass
    #activity.venue.add_field("ContactAddress", venue_address)
    activity.venue.add_field("Postcode", postcode)
    
    activity.add_org()
    activity.org.add_field("DPProviderID", activity.hash(orgname))
    activity.org.add_field("Name", orgname)
    activity.org.add_field("ContactForename", contact_fname)
    activity.org.add_field("ContactSurname", contact_sname)
    activity.org.add_field("ContactEmail", contact_email)
    activity.org.add_field("ContactPhone", contact_phone)
    activity.org.add_field("Website", org_website)
    
    activity.add_field("ActivitySourceID", activity.hash(activity.hash(title)+activity.hash(orgname)+activity.hash(venue_name)+d.strftime("%W%Y")))
    
    activity.keywords.add_field("Keyword", cat1)
    activity.keywords.add_field("Keyword", cat2)
    activity.keywords.add_field("Keyword", cat3)
    activity.keywords.add_field("Keyword", cat4)
    
    i += 1
    #if i>318:
    activity.finish()
    #if i>300: break
    
activities.finish()
print activities.doc.toprettyxml(indent="  ")
    