#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from windmill.authoring import WindmillTestClient
import BeautifulSoup
import pickle
import re, urlparse

def test_scrape_riding():
    client = WindmillTestClient(__name__)
    client.open(url='http://events.eastriding.gov.uk/default.aspx')
    
    links = []
    for val in [7,8]:
        client.waits.forPageLoad()
        client.waits.forElement(id="ctl00_cphMaster_ddlMonths", timeout=30000)
        client.select(id="ctl00_cphMaster_ddlMonths", val=val)
        client.click(id="ctl00_cphMaster_btnSubmit")
        while True:
            client.waits.forPageLoad()
            client.waits.forElement(xpath=u"//table[@class='dgEvents']", timeout=30000)
            response = client.commands.getPageText()
            html = response["result"]
            page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            nav = {}
            current = None
            for tr in page1.find(attrs={"class":"dgEvents"}).findAll("tr"):
                if tr["class"] == "alpha" or tr["class"] == "beta":
                    print tr.a.contents[0]
                    print tr.a["href"]
                    links.append(str(tr.a["href"]))
                elif tr["class"] == "pagerlink":
                    for a in tr.td.findAll("a"):
                        try: nav[int(a.contents[0])] = a["href"]
                        except ValueError: pass
                    current = int(tr.td.span.contents[0])
            
            file = open("east_riding.pickle", "w")
            pickle.dump(links, file)
            file.close
            
            if current+1 in nav:
                j = nav[current+1][len("javascript:"):]
                print j
                client.execJS(js=j)
            else:
                break
    
