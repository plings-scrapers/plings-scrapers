import http_scraper
import re, datetime
from plings_sax import Activities

class Scraper(http_scraper.Scraper):
    def __init__(self):
        self.idre = re.compile(".*?/([A-Za-z0-9\-]+)/$")
        self.baseurl = "http://www.b-active.info"
        self.Activities = Activities
    
    def get(self):
        date = datetime.date.today()
        while date < (datetime.date.today()+datetime.timedelta(weeks=8)):
            url = "/whats_on/day/?y="+str(date.year)+"&m="+str(date.month)+"&d="+str(date.day)
            print url
            (id, page) = self.get_page(url, id=str(date))
            for a in page.find("table", id="calendar").findAll("a"):
                url = a["href"]
                self.http_cache(url, self.idre.match(url).group(1), "activity")
            date += datetime.timedelta(days=1)
        
    def scrape_page(self, id, html, page):
        def row(key):
            try:
                return self.bs_following(page.find("table", "bactive"), key, False).contents[0]
            except AttributeError:
                return ""
        
        activity = self.activities.create_activity()
        activity._ActivitySourceID = id
        activity._Name = page.title.contents[0].split("::")[1].strip()
        
        print row("Contact:")
        if not self.autoaddress(activity, row("Venue:")): return
        activity.venue.hash_name()
        
        activity.keywords._Keyword = row("Type:")
        activity._Starts = self.nice_date(row("Start date:")+" "+row("Start time:"))
        activity._Ends = self.nice_date(row("Start date:")+" "+row("End time:"))
        activity._Description = row("Who is this event for:") + "<br/>\n<br/>\n<a href=\"http://www.b-active.info/whats_on/"+id+"\">See this event on B-Active.info.</a>"
        
        activity.finish()
        print ".",
        
        return
        activity._Description = self.bs_string(page.find("div", "articlecontentwrapper").findAll("div")[1]) + \
            "<br/><br/>\n\n<a href=\"http://www.mogozout.org/Activity/Details/"+id+"\">See this activity on Mogozout</a>"
        
        venue = self.bs_following(page, "Venue:")
        if not self.autoaddress(activity, venue): return
        activity.venue.hash_name()
        
        timing = self.bs_following(page, "Timing and Repeats:")
        self.autodate(activity, timing)
    