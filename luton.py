#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import sys
import os

def deflist_to_dict(dl):
    data = {}
    key = ""
    for tag in dl.findAll(["dt", "dd"]):
        if tag.name == "dt":
            key = "".join(tag.findAll(text=True))
        elif tag.name == "dd":
            data[key] = "".join(tag.findAll(text=True))
    return data

cachedir = "cache/luton/"
baseurl = "http://www.youth.luton.gov.uk/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    d = datetime.date.today()
    while d < datetime.date(2010, 8, 31):
        url = baseurl + "13.cfm?s=13&m=562&p=669,view_day&yy=2010&mm=5&dd=1"
        id = d.isoformat()
        if not id+".html" in os.listdir(cachedir):
            f = open(os.path.join(cachedir, id+".html"), "w")
            html = urllib2.urlopen(url).read()
            f.write(html)
            f.close()
        d += datetime.timedelta(days=1)
                
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "luton.xml"
    
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            date = file[:-len(".html")]
        d = dateutil.parser.parse(date)
        print date
        
        html = open(os.path.join(cachedir, file)).read()
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        activity = activities.create_activity()
        activity.add_field("ActivitySourceID", "no")
        
        for table in page.findAll("table", attrs={"class": "calDayTable"}):
            print table.find(attrs={"class": "calDayHeader"}).contents[0]
            print table
            print
        
        break