#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import sys
import pickle

if len(sys.argv) < 2:
    print "Usage: python picklecat.py file.pickle"
    sys.exit(0)

file = open(sys.argv[1], "r")
data = pickle.load(file)
print data
print len(data)