#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings import Activities
import csv
import re
import datetime

def decode_time(time):
    time = time.strip()
    return datetime.datetime.strptime(time, "%H:%M").time()

spread = csv.reader((open("islington_raw.csv", "r")))
head = spread.next()

activities = Activities()
activities.outname = "islington.xml"

for venue_name, venue_address, project_name, session_title, contact_number, \
            age_range, date, start_time, end_time, session_id in spread:
    activity = activities.create_activity()
    activity.venue.add_field("Name", venue_name)
    activity.venue.add_field("ProviderVenueID", activity.hash(venue_name))
    
    try:
        r = re.search("\S+ \S+\Z", venue_address)
        activity.venue.add_field("Postcode", r.group())
        r = re.match("\d+", venue_address)
        activity.venue.add_field("BuildingNameNo", r.group())
    except: pass
    
    activity.add_field("Description", project_name)
    r = re.match("(.*)\((.*)\)", session_title)
    activity.add_field("Name", r.group(1))
    day = r.group(2)
    activity.add_field("ContactNumber", contact_number)
    try:
        ages = age_range.split("-")
        activity.add_field("MinAge", ages[0])
        activity.add_field("MaxAge", ages[1])
    except: pass
    # Ignore date
    activity.add_field("ActivitySourceID", session_id)
    activity.setup_recurring(day, [decode_time(start_time), decode_time(end_time)])
    activity.venue.add_field("ContactPhone", "0207 527 5641")

    activity.finish()    
    activity.recur()
    
activities.finish()
print activities.doc.toprettyxml(indent="  ")