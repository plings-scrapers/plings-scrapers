#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings_soap import Activities
import csv
import re
import datetime
import config

def decode_date(date):
    date = date.strip()
    return datetime.datetime.strptime(date, "%d/%m/%Y").date()

def decode_time(time):
    time = time.strip()
    return datetime.datetime.strptime(time, "%H:%M").time()

spread = csv.reader((open("HG Target Life Autumn 10.csv", "r")))
head = spread.next()

activities = Activities()
activities.register(config.keys["stockport_sport"]+"-L")

idstem = "HG Target Life Autumn 10"

i=0
for date, starts, ends, venue, venueid, name, description, contact_name, contact_number, project, orgid, minage, maxage \
        in spread:
    i+=1
    activity = activities.create_activity()
    activity.add_field("ActivitySourceID", idstem+"-"+str(i))
    d = decode_date(date)
    activity.add_field("Starts", datetime.datetime.combine(d, decode_time(starts)))
    activity.add_field("Ends", datetime.datetime.combine(d, decode_time(ends)))
    activity.add_field("Name", name)
    activity.add_field("Description", description)
    activity.add_field("ContactName", contact_name)
    activity.add_field("ContactNumber", contact_number)
    activity.add_field("MinAge", minage)
    activity.add_field("MaxAge", maxage)
    activity.add_field("VenueID", venueid)
    #activity.keywords.add_field("Keyword", "swimming")
    activity.categories.add_field("Category", "sports")
    activity.finish()
    
