#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import csv
from plings_sax import Activities
import datetime

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "utf-8") for cell in row]

spread = unicode_csv_reader(open("Norfolk Positive Activities MASTER.csv"))
activities = Activities()
header = spread.next()

def timeparse(time):
    try:
        d = datetime.datetime.strptime(time, "%H:%M")
    except ValueError:
        d = datetime.datetime.strptime("00:00", "%H:%M")
    return d.time()
    
def dateparse(date):
    try:
        d = datetime.datetime.strptime(date, "%d/%m/%Y")
        return d.date()
    except:
        return None

def dbf(name, value):
    if value=="y": return name+": Yes;  "
    elif value=="n": return name+": No;  "
    else: return ""

activities.outname = "norfolk.xml"
for tag, dcsf, checked, id, name, description, venue, venue_id, venue_address, postcode, \
        keywords, startdate, enddate, days, freq, starts, ends, contact_name, contact_number, contact_email, link, \
        org, minage, maxage, capacity, cost_text, cost_numeric, ans, category, locality,  \
        atoilet, dparking, wheelchair, notes in spread:
    activity = activities.create_activity()
    activity.add_field("ActivitySourceID", id)
    activity.add_field("Name", name)
    if capacity: description += "\n\nCapacity:"+capacity
    activity.add_field("Description", description)
    activity.add_field("ContactName", contact_name)
    activity.add_field("ContactNumber", contact_number)
    activity.add_field("ContactEmail", contact_number)
    activity.add_field("MinAge", minage)
    activity.add_field("MaxAge", maxage)
    activity.add_field("Cost", cost_numeric)
    activity.venue.add_field("ProviderVenueID", venue_id)
    activity.venue.add_field("Name", venue)
    activity.venue.add_field("Postcode", postcode)
    activity.venue.add_field("DisabledFacilitiesNotes", dbf("Adapted Toilet", atoilet)+dbf("Disabled Parking", dparking)+dbf("Wheelchair Access", wheelchair))
    activity.taxonomy.add_field("norfolk", category)
    activity.keywords.add_field("Keyword", keywords)
    activity.taxonomy.add_field("norfolk-pa", tag)
    activity.taxonomy.add_field("norfolk-dcsf", dcsf)
    activity.taxonomy.add_field("norfolk-checkeddate", checked)
    activity.add_org()
    activity.org.add_field("DPProviderID", activity.hash(org))
    activity.org.add_field("Name", org)
    for antag in ans.split(","):
        activity.taxonomy.add_field("active_norfolk", antag)
    #TODO cost-text notes
    if days and freq=="Weekly":
        for day in days.split(","):
            try:
                activity2 = activities.cloneactivity(activity, id+"-"+day)
                end = dateparse(enddate)
                #if end > datetime.date(2010, 9, 30): end = datetime.date(2010, 8, 30)
                activity2.setup_recurring(day, map(timeparse, [starts, ends]), dateparse(startdate), end)
                activity2.finish()
                activity2.recur()
            except: continue
    elif freq:
        print "Error: "+id+" - Frequency "+freq+" not supported."
    else:
        print "Error: "+id+" - No day."
    
activities.finish()