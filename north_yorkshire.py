#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import sys
import os
import csv

cachedir = "cache/north_yorkshire/"
if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    next = [ ("http://www.gimi.co.uk/index.aspx?articleid=7602", 0) ]
    while True:
        if len(next):
            (url, level) = next.pop()
        else:
            break
        print url
        html = urllib2.urlopen(url).read()
        page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        try:
            for li in page1.find("div", id="list").ul.findAll("li"):
                link = str(li.a["href"])
                if link[0:7] != "http://": link = "http://www.gimi.co.uk/" + link
                if level == 2:
                    m = re.search("articleid=([0-9]+)", link)
                    id = m.group(1)
                    if not id+".html" in os.listdir(cachedir):
                        print id
                        f = open(os.path.join(cachedir, id+".html"), "w")
                        f.write(urllib2.urlopen(link).read())
                        f.close()
                else:
                    next.append((link, level+1))
        except AttributeError: pass
        except urllib2.HTTPError: print "HTTPError!"
        
elif sys.argv[1] == "scrape":
    out = csv.writer(open("north_yorkshire.csv", "w"))
    
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        
        try:
            print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            row = []
            row.append(id)
            row.append( page.find("div", attrs={"class":"main-column"}).find("h1").contents[0] )
            row.append( page.find("div", attrs={"class":"main-column"}).find("h2").contents[0] )
            data = {}
            key = ""
            for bit in page.find(id="eventinfo").contents:
                if bit.__class__.__name__ == "NavigableString":
                    s = str(bit).strip()
                    if s:
                        data[key] = s
                        row.append(s)
                elif bit.name == "a":
                    s = bit.getString()
                    if s:
                        data[key] = s
                        row.append(s)
                elif bit.name == "span":
                    key = bit.getString().strip(" :")
                    
            print row
            out.writerow(row)
        except:
            print "Fail"
        
    
    