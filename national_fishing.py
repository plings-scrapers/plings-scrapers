#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import os, sys

def dateparse(date):
    try:
        d = dateutil.parser.parse(date)
    except: raise Exception("Invalid date - "+date)
    return d.date()
    
def timeparse(time):
    try:
        d = dateutil.parser.parse(time)
    except: raise Exception("Invalid time - "+time)
    return d.time()

cachedir = "cache/national_fishing/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    url = "http://www.nationalfishingweek.co.uk/events/?postcode=M1+2EJ&distance=2147483647&x=50&y=11"
    html = urllib2.urlopen(url).read()
    page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)

    for h3 in page1.findAll("h3"):
        m = re.match(".*event=([0-9]*)\&", h3.a["href"])
        id = m.group(1)
        url = "http://www.nationalfishingweek.co.uk/events/event.php?event="+id
        
        if not id+".html" in os.listdir(cachedir):
            print id
            f = open(os.path.join(cachedir, id+".html"), "w")
            f.write(urllib2.urlopen(url).read())
            f.close()

elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "national_fishing.xml"
    
    j=0
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        
        try:
            print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            fields = {}
            pc = False
            for p in page.findAll('p'):
                label = p.find(attrs={"class":"label"})
                try:
                    if pc:
                        pc -= 1
                        if not pc:
                            fields["Postcode: "] = "".join(p.contents[1].findAll(text=True))
                            continue
                    if not "desc" in fields and label.contents[0] == u"\u00A0":
                        fields["desc"] = p.contents[1]
                    else:
                        if p.contents[1].__class__.__name__ == "NavigableString":
                            fields[label.strong.contents[0]] = str(p.contents[1])
                        else:
                            fields[label.strong.contents[0]] = "".join(p.contents[1].findAll(text=True))
                    if label.strong.contents[0] == "Event Location: ": pc = 4
                except: pass
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            activity.add_field("Name", page.h3.contents[0])
            if not "desc" in fields: fields["desc"] = ""
            if not "Introduction: " in fields: fields["Introduction: "] = ""
            if not "Additional Notes " in fields: fields["Additional Notes "] = ""
            if not "Additional Comments " in fields: fields["Additional Comments "] = ""
            activity.add_field("Description", fields["desc"]+"\n\n"+fields["Introduction: "]+"\n\n"+fields["Additional Notes "]+"\n\n"+fields["Additional Comments "])
            try: activity.add_field("ContactName", fields["Contact: "])
            except KeyError: pass
            try: activity.add_field("ContactNumber", fields["Contact Telephone: "])
            except KeyError: pass
            d = dateparse(fields["Date: "])
            times = fields["Times: "].split("-")
            if len(times) == 1: times = times[0].split("to")
            activity.add_field("Starts", datetime.datetime.combine(d, timeparse(times[0])).strftime("%Y-%m-%dT%H:%M:%S"))
            activity.add_field("Ends", datetime.datetime.combine(d, timeparse(times[1])).strftime("%Y-%m-%dT%H:%M:%S"))
            activity.venue.add_field("ProviderVenueID", activity.hash(fields["Event Location: "]))
            activity.venue.add_field("Name", fields["Event Location: "])
            activity.venue.add_field("Postcode", fields["Postcode: "])
            activity.keywords.add_field("Keyword", "angling")
            activity.finish()
        except Exception as e:
            #help(e)
            print e.__class__.__name__, id,  e
            #print "Error: "+id

    activities.finish()
