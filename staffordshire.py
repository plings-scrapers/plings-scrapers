#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings_sax import Activities
import csv
import datetime
import dateutil.parser
import string
import re

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "utf-8") for cell in row]

def dateparse(date):
    return dateutil.parser.parse(date).date()

def timeparse(time):
    return dateutil.parser.parse(time).time()

spread = unicode_csv_reader((open("Staffordshire-2010.11.06.csv", "r")))
head = spread.next()

activities = Activities()
activities.outname = "staffordshire.xml"

for row in spread:
    orgname, orgid, orgpostcode, orgemail, orgweb, status, acc, pol, ins, venuename, venueaddr1, venueaddr2, \
        venuetown, venuecounty, venuepostcode, district, map, canteen, kitchen, wheelchair, dtoilets, loop, toilets, \
        osa, isa, parking, internet, activitytitle, activityid, activitydesc, catname, subcatname, catcode, \
        acttype, minage, maxage, startdate, enddate, specialneeds, isfree, cost, contactname, contactemail, \
        contacttel1, contacttel2, consent, unsuit, search, oneoffstarts, oneoffends = row[:50]
    i=0
    rec = []
    occ = {}
    for field in row[50:]:
        if i%5 == 0:
            if field == "":
                break
            else:
                occ["day"] = field
        elif i%5 == 1: occ["freq"] = field
        elif i%5 == 2: occ["termonly"] = field
        elif i%5 == 3: occ["starts"] = field
        elif i%5 == 4:
            occ["ends"] = field
            rec.append(occ)
        i+=1
    
    activity = activities.create_activity()
    activity.add_field("ActivitySourceID", activityid)
    activity.add_field("Name", string.capwords(activitytitle))
    activity.add_field("Description", activitydesc)
    activity.add_field("ContactName", contactname)
    activity.add_field("ContactNumber", contacttel1+contacttel2)
    activity.add_field("ContactEmail", contactemail)
    activity.add_field("MinAge", minage)
    activity.add_field("MaxAge", maxage)
    
    activity.venue.add_field("ProviderVenueID", activity.hash(string.capwords(venuename)))
    activity.venue.add_field("Name", string.capwords(venuename))
    m = re.match("[0-9]+", venueaddr1)
    if m:
        activity.venue.add_field("BuildingNameNo", m.group(0))
    activity.venue.add_field("Postcode",  venuepostcode)
    activity.venue.add_field("DisabledFacilitiesNotes", "Wheelchair Access: "+wheelchair+"; DisabledToilets: "+dtoilets+"; Induction loop: "+loop)
    
    activity.add_org()
    activity.org.add_field("DPProviderID", orgid)
    activity.org.add_field("Name", string.capwords(orgname))
    activity.org.add_field("BuildingNameNo", string.capwords(orgname))
    activity.org.add_field("Postcode", orgpostcode)
    activity.org.add_field("ContactEmail", orgemail)
    activity.org.add_field("Website", orgweb)
    
    activity.keywords.add_field("Keyword", catname)
    activity.keywords.add_field("Keyword", subcatname)
    
    dates = [dateparse(x) for x in [startdate, enddate]]
    if oneoffstarts and oneoffstarts != "N/A":
        try:
            activity.add_field("Starts", datetime.datetime.combine(dates[0], timeparse(oneoffstarts)).isoformat())
            activity.add_field("Ends", datetime.datetime.combine(dates[1], timeparse(oneoffends)).isoformat())
        except ValueError: continue
        activity.finish()
        
    else:
        for occ in rec:
            try:
                if occ["freq"] == "Weekly" and occ["termonly"] == "No":
                    day = occ["day"]
                    times = [timeparse(x) for x in [occ["starts"], occ["ends"]]]
                    activity2 = activities.cloneactivity(activity, activityid+"-"+day)
                    activity2.setup_recurring(day, times, dates[0], dates[1])
                    activity2.finish()
                    activity2.recur()
            except ValueError: continue
    
    """
        status, acc, pol, ins, venuename, venueaddr1, venueaddr2, \
        venuetown, venueconty, district, map, canteen, kitchen, toilets, \
        osa, isa, parking, internet, catcode, \
        acttype, specialneeds, isfree, cost, 
        consent, unsuit, search, oneoffstarts, oneoffends
     """
     
activities.finish()
