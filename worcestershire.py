#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings_sax import Activities
import datetime
import dateutil.parser
import re
import sys
import os

cachedir = "cache/worcestershire/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    next = None
    j=0
    while True:
        if next:
            url = next
        else:
            url = "http://www.plugandplay.org.uk/events/"
        print url
        html = urllib2.urlopen(url).read()
        page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
    
        next = page1.find(attrs={"title":"Browse Forward One Week"})["href"]
        i=0
        for a in page1.findAll("a", attrs={"class":"eventListTitle"}):
            m = re.match(".*ID=([0-9]+)", a["href"])
            id = m.group(1)
            if not id+".html" in os.listdir(cachedir):
                print id
                f = open(cachedir+id+".html", "w")
                f.write(urllib2.urlopen(a["href"]).read())
                f.close()
                i+=1
                #if i>1:
                #    break
                
        j+=1
        if j>20: break

elif sys.argv[1] == "scrape":    
    activities = Activities()
    activities.outname = "worcestershire.xml"
    
    j=0
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        try:
            print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            activity.add_field("Name", page.find(attrs={"class":"eventDetailTitle"}).contents[0])
            activity.add_field("Description", "\n".join(page.find(attrs={"class":"eventDetailDesc"}).findAll(text=True))+"\n<a href=\"http://www.plugandplay.org.uk/events/index.php?com=detail&eID="+id+"\">See the event on Worcestershire's website</a>")
            datetext = page.find(attrs={"class":"eventDetailDate"}).contents[0]
            timetext = page.find(attrs={"class":"eventDetailTime"}).contents[0]
            times = timetext.split(" - ")
            d = dateutil.parser.parse(datetext).date()
            activity.add_field("Starts", datetime.datetime.combine(d, dateutil.parser.parse(times[0]).time()).isoformat())
            activity.add_field("Ends", datetime.datetime.combine(d, dateutil.parser.parse(times[1]).time()).isoformat())
            
            l = page.find(id="eventDetailInfo").find("b", text="Location")
            n = l.parent
            for i in range(0,7):
                if i==2:
                    activity.venue.add_field("Name", unicode(n))
                    activity.venue.add_field("ProviderVenueID", activity.hash(unicode(n)))
                elif i==6:
                    m = re.match(".*?([A-Z0-9]+ [A-Z0-9]+)", unicode(n))
                    activity.venue.add_field("Postcode", m.group(1))
                n = n.nextSibling
                #print n
            
            info = "".join(page.find(id="eventDetailInfo").findAll(text=True))
            m = re.search("Phone: ([0-9\(\) ]*)", info)
            if m:
                activity.add_field("ContactNumber", str(m.group(1)).translate(None, "() "))
            try:
                script = page.find(id="eventDetailInfo").script.contents[0]
                m = re.search("ename = '(.*?)'", script)
                p1 = m.group(1)
                m = re.search("edomain = '(.*?)'", script)
                p2 = m.group(1)
                print p1+"@"+p2
                activity.add_field("ContactEmail", p1+"@"+p2)
            except: pass
            
            activity.finish()
        except AttributeError: pass
        except ValueError: pass
        j+=1
        #if j>10:
        #    break
   
    activities.finish()