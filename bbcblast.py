#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import hashlib
import datetime
import csv
import dateutil.parser

def decode_time(time):
    d = dateutil.parser.parse(time)
    return d.time()
    
def decode_date(date):
    d = dateutil.parser.parse(date)
    return d.date()

import sys
if len(sys.argv) < 2:
    print "Usage: python bbcplast.py id"
    sys.exit(0)
id = sys.argv[1]

url = "http://www.bbc.co.uk/blast/"+id
html = urllib2.urlopen(url).read()
#html = open(id).read()
page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)

out = csv.writer(open("bbcblast_"+id+".csv", "w"))

for table in page.findAll(attrs={"class":"grey-border events"}):
    date = decode_date(table.previous.previous.strip()+" 2010")
    for tr in table.findAll("tr"):
        tds = tr.findAll("td")
        if len(tds) == 3:
            row = []
            # Time
            times = map(decode_time, tds[0].contents[0].split("-"))
            row.append(date)
            row.append( times[0].strftime("%H:%M") )
            row.append( times[1].strftime("%H:%M") )
            # Title
            row.append( tds[1].h3.renderContents() )
            # Description
            row.append( tds[1].p.renderContents() )
            # Status
            if tds[2].form != None:
                row.append( "Booking Required" )
            else:
                row.append( tds[2].renderContents() )
            out.writerow(row)
