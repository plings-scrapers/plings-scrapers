import http_scraper
import re, datetime
from plings_sax import Activities

class Scraper(http_scraper.Scraper):
    def __init__(self):
        self.idre = re.compile(".*?/([A-Za-z0-9\-]+)$")
        self.baseurl = "http://www.mogozout.org"
        self.Activities = Activities
    
    def get(self):
        for i in range(1,100):
            (id, page) = self.get_page("/Search/Results?Category=None&Where=&What=&x=27&y=21&page="+str(i))
            for div in page.findAll("div", "listeritemimage"):
                url = div.find("a")["href"]
                self.http_cache(url, self.idre.match(url).group(1), "activity")
        
    def scrape_page(self, id, html, page):
        activity = self.activities.create_activity()
        activity._ActivitySourceID = id
        activity._Name = page.title.contents[0].split("|")[0]
        
        activity._Description = self.bs_string(page.find("div", "articlecontentwrapper").findAll("div")[1]) + \
            "<br/><br/>\n\n<a href=\"http://www.mogozout.org/Activity/Details/"+id+"\">See this activity on Mogozout</a>"
        
        venue = self.bs_following(page, "Venue:")
        if not self.autoaddress(activity, venue): return
        activity.venue.hash_name()
        
        timing = self.bs_following(page, "Timing and Repeats:")
        self.autodate(activity, timing)
    