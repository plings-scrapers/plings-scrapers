#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.

from suds.client import Client
import datetime
from plings_sax import Activities

mapping_a = \
    {
        "Name": "EventTitle",
        "Starts": "startDate",
        "Ends": "endDate",
        "Description": "EventDescription",
        "ContactEmail": "EventContactEmail",
        "ContactNumber": "EventContactPhone",
        "MinAge": "FromAge",
        "MaxAge": "ToAge",
        "Cost": "Cost"
    }
mapping_v = \
    {
        "Name": "EventTitle",
        "ContactForename": "EventContactForeName",  
        "ContactSurname": "EventContactSurName",
        "ContactEmail": "EventContactEmail",
        "ContactPhone": "EventContactPhone",
        "ContactFax": "EventContactFax",
        "BuildingNameNo": "EventAddress1",
        "Street": "EventAddress2",
        "Town": "EventAddress3",
        "County": "EventCounty",
        "Postcode": "EventPostcode"
    }
mapping_o = \
    {
        "DPProviderID": "ProviderID",
        "Name": "ProviderName",
        "Description": "ProviderDescription"
    }

activities = Activities()
activities.outname = "bolton.xml"

client = Client("http://www.localdirectory.bolton.gov.uk/webservices/localdirectorywebservices.asmx?WSDL")
for dt in (datetime.datetime.today() + datetime.timedelta(n) for n in range(30)):
    for cat in [31, 8, 21, 23]:
        try:
            response = client.service.GetAllEventsByDate(cat, "", dt)
            for event in response.EventActivityDetails:
                activity = activities.create_activity()
                activity.add_field("ActivitySourceID", unicode(event.EventID)+"-"+unicode(event.startDate))
                activity.add_org()
                for k,v in mapping_a.items():
                    if hasattr(event, v):
                        activity.add_field(k, getattr(event, v))
                for k,v in mapping_v.items():
                    if hasattr(event, v):
                        activity.venue.add_field(k, getattr(event, v))
                for k,v in mapping_o.items():
                    if hasattr(event, v):
                        activity.org.add_field(k, getattr(event, v))
                
                if hasattr(event, "EventContactForeName") and hasattr(event, "EventContactSurName"):
                    activity.add_field("ContactName", event.EventContactForeName +" "+ event.EventContactSurName)
                activity.venue.add_field("ProviderVenueID", activity.hash(activity.venue.name))
                if hasattr(event, "EventCyclePark"):
                    if event.EventCyclePark:
                        activity.venue.add_field("CyclePark", "Yes")
                    else:
                        activity.venue.add_field("CyclePark", "No")
                
                activity.finish()
        except Exception, e: print e
    
activities.finish()
