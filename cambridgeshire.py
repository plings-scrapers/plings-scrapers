import http_scraper
import re, datetime
from plings_sax import Activities

class Scraper(http_scraper.Scraper):
    def __init__(self):
        self.idre = re.compile(".*?([0-9]+)\.aspx$")
        self.baseurl = "http://www.cambridgeshire.net"
        self.Activities = Activities
    
    def get(self):
        for i in range(1,14):
            (id, page) = self.get_page("/events/Results.aspx?audience=Young%20people&PageNumber="+str(i))
            for a in page.find("div", "resultsLists").findAll("a"):
                url = a["href"].replace("../", "/")
                self.http_cache(url, self.idre.match(url).group(1), "activity")
        
    def scrape_page(self, id, html, page):
        activity = self.activities.create_activity()
        activity._ActivitySourceID = id
        activity._Name = page.title.contents[0]
        
        # Timing information
        when = self.bs_following(page, "When ")
        days = self.extract_days(when)
        times = self.extract_times(when)
        if not times: return
        
        # Description
        desc = self.bs_following(page, "About this activity")
        desc += "<br/><br/>\n\n<a href=\"http://www.cambridgeshire.net/activity/"+id+".aspx\">See this activity on cambridgeshire.net</a>"
        activity.add_field("Description", desc)
        
        # Contact info
        contact = page.find("div", "box-hlg-sc")
        activity._ContactName = self.bs_following(contact, "Name")
        activity._ContactNumber = self.bs_following(contact, "Tel")
        
        # Venue address
        # (There is a 'venue address' on the activity page AND the organisation
        #   page, not sure which one to choose)
        try:
            address = page.find("div","eventVenue").find("address")
            activity.venue._Name = address.contents[0]
            self.autoaddress_street(activity, address.contents[2])
            activity.venue._Town = address.contents[4]
            if len(address.contents) > 10:
                activity.venue._PostTown = address.contents[6]
            activity.venue._County = address.contents[-3]
            activity.venue._Postcode = address.contents[-1]
            activity.venue.hash_name()
        except AttributeError, e:
            print e
            return
        
        ## Organisation
        orgurl = self.bs_following(page, "Run by", toString=False).find("a")["href"]
        orgurl = orgurl.replace("../../", "/").strip()
        (id, page) = self.get_page(orgurl, type="org", idre=re.compile(".*?([0-9]+)\.aspx$"))
        activity.add_org()
        activity.org._DPProviderID = id
        activity.org._Name = page.title.contents[0]
        activity.org._Description = self.bs_following(page, "About this organisation")
        
        # Organisation contact details
        contact = page.find("div", "box-hlg-sc")
        names = self.bs_following(contact, "Name").split(" ")
        if len(names) > 1:
            activity.org._ContactForename = names[0]
            activity.org._ContactSurname = " ".join(names[1:])
        activity.org._ContactPhone = self.bs_following(contact, "Tel")
        
        activity.multiple_recur(self.activities, days, times)
    