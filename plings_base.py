#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import copy
import datetime
import hashlib

startdate = datetime.date.today()
enddate = datetime.date(2011, 04, 15)

def isint(num):
    try:
        if int(num) == num:
            return True
        else:
            return False
    except ValueError:
        return False

def day2int(day):
    mapping = {
        0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6,
        "monday":0, "tuesday":1, "wednesday":2, "thursday":3, "friday":4, "saturday":5, "sunday":6,
        "mon":0, "tue":1,  "wed":2, "thur":3, "fri":4, "sat":5, "sun":6,
        "wedne":2, "satur":5
    }
    try:
        return mapping[day.lower().rstrip("s")]
    except KeyError:
        raise ValueError("Not a valid day - "+day)

class Thing:
    required_fields = []
    placeholders = {}
    
    """
    class F:
        def __setattr__(self, name, value):
            parent.add_field(name, value
    f = F()
    """
    
    def __setattr__(self, name, value):
        self.__dict__[name] = value
        if name[0] == "_":
            self.add_field(name[1:], value)
    
    def hash(self, text):
        return hashlib.sha1(text.strip().encode("utf8")).hexdigest()
    
    def formatdate(self, date):
        return date.strftime("%Y-%m-%dT%H:%M:%S")
    
    #def add_field(self, name, contents): pass
    
    #def update_field(self, name, contents): pass
    
    def try_placeholder(self, field):
        try:
            self.add_field(field, self.placeholders[field])
        except KeyError:
            print "Error: Required field '"+field+"' is missing. ("+unicode(self.id)+", "+unicode(self.name)+")"
    
    #def finish(self): pass

class Venorg(Thing):
    def try_placeholder(self, field):
        if field == "BuildingNameNo": self.add_field("BuildingNameNo", self.name)
        elif field == "ContactForename":
            self.forenamemissing = True
        elif field == "ContactSurname":
            if self.forenamemissing:
                self.add_field("ContactForename", "General")
                self.add_field("ContactSurname", "enquiries")
            else:
                self.add_field("ContactSurname", "-")
        else: Thing.try_placeholder(self, field)

class Venue(Venorg):
    required_fields = ["ProviderVenueID", "Name", "BuildingNameNo", "Postcode", "ContactForename", "ContactSurname", "ContactPhone" ]
    placeholders = {"ContactPhone":"n/a","ContactForename":"-","ContactSurname":"-"}
    forenamemissing = False
    
    def hash_name(self):
        self.add_field("ProviderVenueID", self.hash(self.name))

class Organisation(Venorg):
    required_fields = ["DPProviderID", "Name", "BuildingNameNo", "ContactForename", "ContactSurname", "ContactPhone" ]
    placeholders = {"ContactPhone":"n/a","ContactForename":"-","ContactSurname":"-"}
    forenamemissing = False

class Activity(Thing):
    required_fields = ["ActivitySourceID", "Name", "Description", "Starts", "Ends", "ContactName", "ContactNumber"]
    placeholders = {"ContactName":"General enquiries", "ContactNumber":"n/a", "Description":"-"}
    recur_time = None
    recur_day = None
    recur_currdate = None
    recur_count=0
    org = None
    
    disable_venue = False
    
    #def add_org(self): pass
    
    def setup_recurring(self, day, time, starts=None, ends=None):
        self.recur_time = time
        if isint(day):
            self.recur_day = day
        else:
            self.recur_day = day2int(day)
        if starts and starts > startdate: self.startdate = starts
        else: self.startdate = startdate
        if ends: self.enddate = ends
        else: self.enddate = enddate
        self.recur_currdate = self.startdate
        while True:
            if self.recur_currdate.weekday() == self.recur_day: break
            self.recur_currdate += datetime.timedelta(days=1)
        self.seriesid = self.id
        self.update_field("ActivitySourceID", self.seriesid+"-"+self.recur_currdate.strftime("%W%Y"))
        self.add_field("Starts", datetime.datetime.combine(self.recur_currdate, self.recur_time[0]).strftime("%Y-%m-%dT%H:%M:%S"))
        self.add_field("Ends", datetime.datetime.combine(self.recur_currdate, self.recur_time[1]).strftime("%Y-%m-%dT%H:%M:%S"))
    
    def multiple_recur(self, aaa, days, time, starts=None, ends=None):
        for day in days:
            if not isint(day):
                day = day2int(day)
            activity2 = aaa.cloneactivity(self, self.id+"-"+str(day))
            activity2.setup_recurring(day, time, starts, ends)
            activity2.recur()
            activity2.finish()
    
    #def recur(self): pass
