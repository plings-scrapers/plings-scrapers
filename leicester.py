#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings import Activities
import csv
import datetime
import dateutil.parser

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "utf-8") for cell in row]

def timeparse(time):
    return dateutil.parser.parse(time).time()

activities = Activities()
activities.outname = "leicester.xml"

for filename in ["Leicester Export 23 June 2010.csv", "Holiday Activities 15072010.csv", "New Holiday Activities 27072010.csv"]:
    spread = unicode_csv_reader(open(filename, "r"))
    head = spread.next()
    
    i=1
    for title, category, times, internaldate, startdate, enddate, venue_name, venue_address, postcode, cost, \
    description, \
            agerange, quality, website, contact_name, contact_address, contact_number, contact_email, \
            type_provider, type_activity, url, keywords, videourl, recordactivityrange, source, externalid in spread:
        
        i+=1
        
        activity = activities.create_activity()
        activity.add_field("ActivitySourceID", externalid)
        activity.add_field("Name", title)
        activity.add_field("Description", description)
        activity.add_field("ContactName", contact_name)
        activity.add_field("ContactNumber", contact_number)
        activity.add_field("ContactEmail", contact_email)
        activity.add_field("ContactAddress", contact_address)
        activity.keywords.add_field("Keyword", category)
        activity.venue.add_field("ProviderVenueID", activity.hash(venue_name))
        activity.venue.add_field("Name", venue_name)
        activity.venue.add_field("Postcode", postcode)
        
        #print map(unicode.strip, type_provider.split("/"))
        types = map(unicode.strip, type_activity.split("/"))
        dcsf = types[0].lower().replace(" provision", "").replace(" and active leisure", "")
        if dcsf == "youth club":
            if "organised" in types[1]: dcsf += " - organised"
            if "drop in" in types[1]: dcsf += " - drop in"
        print dcsf
        activity.taxonomy.add_field("dcsf", dcsf)
        
        if startdate: starts = dateutil.parser.parse(startdate).date()
        else: starts = None
        if enddate: ends = dateutil.parser.parse(enddate).date()
        else: ends = None
        
        try:
            timeslist = times.split(" ")
            times = map(timeparse, timeslist[-1].split("-"))
            #print i, times
            for day in timeslist[:-1]:
                day = day.strip(",")
                try:
                    days = day.split("-")
                    d1 = dateutil.parser.parse(days[0]).date()
                    if len(days) > 1:
                        d2 = dateutil.parser.parse(days[1]).date()
                    else:
                        d2 = d1
                    activity.add_field("Starts", datetime.datetime.combine(d1, times[0]).isoformat())
                    activity.add_field("Ends", datetime.datetime.combine(d2, times[1]).isoformat())
                except ValueError:
                    try:
                        activity2 = activities.cloneactivity(activity, externalid+"-"+day)
                        activity2.setup_recurring(day, times, starts, ends)
                        activity2.finish()
                        activity2.recur()
                    except ValueError: continue
        except: continue
    
activities.finish()