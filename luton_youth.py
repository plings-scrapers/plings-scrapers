#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import hashlib
import datetime

def time2datetime(time):
    try:
        d = datetime.datetime.strptime(time, "%I.%M%p")
    except ValueError:
        d = datetime.datetime.strptime(time, "%I%p")
    return d.time()

def scrape(url):
    activity = activities.create_activity()
    print url
    html = urllib2.urlopen(url).read()
    page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
    
    id = hashlib.sha1(page.find("h2").contents[0]).hexdigest()
    activity.add_field("ActivitySourceID", id)
    activity.add_field("Name", page.find("h2").contents[0])
    contact = page.find(id="contactWrapper")
    try:
        activity.venue.add_field("ProviderVenueID", hashlib.sha1(str(contact.address.contents[0]).strip()).hexdigest())
        activity.venue.add_field("Name", str(contact.address.contents[0]))
        activity.venue.add_field("Postcode", str(contact.address.contents[-1]))
    except AttributeError: return
    ps = contact.findAll("p")
    name = ps[0].contents[0]
    try:
        activity.add_field("ContactName", name)
        names = name.split()
        activity.venue.add_field("ContactForename", names[0])
        activity.venue.add_field("ContactSurname", names[1])
        activity.add_field("ContactPhone", ps[1].contents[0][4:])
        activity.venue.add_field("ContactPhone", ps[1].contents[0][4:])
        if len(ps) > 3:
            activity.add_field("ContactEmail", ps[3].a.contents[0])
            activity.venue.add_field("ContactEmail", ps[3].a.contents[0])
    except TypeError: pass
    
    boxinner2 = page.findAll(attrs={"class":"box-inner2"})[1]
    try: trs = boxinner2.table.findAll("tr")
    except AttributeError: return
    boxinner2.table.extract()
    lastday=""
    i=0
    for tr in trs:
        try:
            i+=1
            activity2 = activities.cloneactivity(activity,id+"-"+str(i))
            day = tr.find(attrs={"class":"day"}).contents[0]
            if day=="": day=lastday
            else: lastday = day
            time =  tr.find(attrs={"class":"time"}).contents[0]
            desc = tr.find(attrs={"class":"desc"}).contents[0]
            activity2.add_field("Description", str(boxinner2.contents[0])+"<p>"+str(desc)+"</p>")
            activity2.setup_recurring(day, map(time2datetime, time.split("-")))
            activity2.finish()
            activity2.recur()
            #activities.recur(activity2)
        except: pass

root = "http://www.youth.luton.gov.uk"
url = "http://www.youth.luton.gov.uk/13.cfm?p=582"
html = urllib2.urlopen(url).read()
page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
activities = Activities()
activities.outname = "luton_youth.xml"

boxinner2 = page.find(attrs={"class":"box-inner2"})
i=0
for li in boxinner2.findAll("li"):
    scrape(root+li.a["href"])
    i+=1
    #if i>0: break

activities.finish()
print activities.doc.toprettyxml(indent="  ")
