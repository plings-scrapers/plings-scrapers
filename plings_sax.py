#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from xml.sax.saxutils import XMLGenerator
import plings_base
import copy
import datetime

def dict_to_xml(dict, g):
    for (k,v) in dict.items():
        g.startElement(k, {})
        g.characters(unicode(v))
        g.endElement(k)

class Activities:
    f = None
    
    def __init__(self):
        pass
        
    def start(self):
        self.f = open(self.outname, "w")
        self.f.write('<?xml version="1.0" encoding="UTF-8"?>')
        self.g = XMLGenerator(self.f, 'utf-8')
        self.g.startElement("Activities", {})
        
    def create_activity(self):
        if not self.f: self.start()
        activity = Activity(self.g)
        return activity
    
    def finish(self):
        self.g.endElement("Activities")
        self.g.endDocument()
        self.f.close()
        
    def cloneactivity(self, activity, newid):
        activity2 = copy.deepcopy(activity)
        activity2.update_field("ActivitySourceID", newid)
        return activity2

class Thing(plings_base.Thing):
    data = None
    
    def get_id(self):
        try: return self.data["ActivitySourceID"]
        except KeyError: return "Unknown"
    def set_id(self, value): self.data["ActivitySourceID"] = value
    def get_name(self):
        try: return self.data["Name"]
        except KeyError: return "Unknown"
    def set_name(self, value): self.data["Name"] = value
    id = property(get_id, set_id)
    name = property(get_name, set_name)
    
    def __deepcopy__(self, memo):
        thing2 = copy.copy(self)
        thing2.data = copy.copy(self.data)
        return thing2
    
    def __init__(self, g):
        self.data = {}
        self.g = g

    def add_field(self, name, contents):
        self.data[name] = contents
        
    def update_field(self, name, contents):
        self.data[name] = contents
        
    def finish(self):
        for field in self.required_fields:
            if not field == "ActivitySourceID":
                if not field in self.data or self.data[field].strip(" \r\n") == "":
                    self.try_placeholder(field)

class Venue(plings_base.Venue, Thing):
    def get_id(self):
        try: return self.data["ProviderVenueID"]
        except KeyError: return "Unknown"
    def set_id(self, value): self.data["ProviderVenueID"] = value
    id = property(get_id, set_id)
    def finish(self):
        Thing.finish(self)
        self.g.startElement("Venue", {})
        dict_to_xml(self.data, self.g)
        self.g.endElement("Venue")

class Organisation(plings_base.Organisation, Thing):
    def get_id(self):
        try: return self.data["DPProviderID"]
        except KeyError: return "Unknown"
    def set_id(self, value): self.data["DPProviderID"] = value
    id = property(get_id, set_id)
    def finish(self):
        Thing.finish(self)
        self.g.startElement("ActivityProvider", {})
        dict_to_xml(self.data, self.g)
        self.g.endElement("ActivityProvider")

class Activity(plings_base.Activity, Thing):
    def  __init__(self, g):
        Thing.__init__(self, g)
        self.venue = Venue(self.g)
        self.categories = Thing(self.g)
        self.keywords = Thing(self.g)
        self.taxonomy = Thing(self.g)
    
    def __deepcopy__(self, memo):
        activity2 = copy.copy(self)
        activity2.recur_time = copy.copy(self.recur_time)
        activity2.recur_day = copy.copy(self.recur_day)
        activity2.recur_currdate = copy.copy(self.recur_currdate)
        
        activity2.data = copy.copy(self.data)
        activity2.venue = copy.deepcopy(self.venue)
        activity2.categories = copy.deepcopy(self.categories)
        activity2.keywords = copy.deepcopy(self.keywords)
        activity2.taxonomy = copy.deepcopy(self.taxonomy)
        if self.org:
            activity2.org = copy.deepcopy(self.org)
        return activity2
    
    def add_org(self):
        self.org = Organisation(self.g)
    
    def finish(self):
        Thing.finish(self)
        self.g.startElement("Activity", {})
        dict_to_xml(self.data, self.g)
        self.g.startElement("Categories", {})
        dict_to_xml(self.categories.data, self.g)
        self.g.endElement("Categories")
        self.g.startElement("Keywords", {})
        dict_to_xml(self.keywords.data, self.g)
        self.g.endElement("Keywords")
        self.venue.finish()
        if self.org:
            self.org.finish()
        self.g.endElement("Activity")
        
    def recur(self):
        self.recur_currdate += datetime.timedelta(days=7)
        while self.recur_currdate  <= self.enddate:
            self.recur_count += 1
            activity2 = copy.deepcopy(self)
            activity2.update_field("ActivitySourceID", self.seriesid+"-"+self.recur_currdate.strftime("%W%Y"))
            activity2.update_field("Starts", datetime.datetime.combine(self.recur_currdate, self.recur_time[0]).strftime("%Y-%m-%dT%H:%M:%S"))
            activity2.update_field("Ends", datetime.datetime.combine(self.recur_currdate, self.recur_time[1]).strftime("%Y-%m-%dT%H:%M:%S"))
            activity2.add_field("LinkWithActivitySourceID", self.id)
            activity2.finish()
            self.recur_currdate += datetime.timedelta(days=7)
    