#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from xml.etree.ElementTree import iterparse
import sys
import csv

def node_to_dict(node):
    children = node.getchildren()
    if len(children) == 0: return node.text
    data = node.attrib
    for child in children:
        childdata = node_to_dict(child)
        #if childdata.__class__.__name__ == "str" or childdata.__class__.__name__ == "NoneType":
        if not (child.tag == "keyword" or child.tag == "category"):
            data[child.tag] = childdata
        else:
            if not data.has_key(child.tag):
                data[child.tag] = []
            data[child.tag].append(childdata)
    return data

class Results():
    itp = None
    root = None
    
    def __init__(self, f):
        self.itp = iter(iterparse(f, events=['start']))
    
    def __iter__(self):
        while True:
            event, node = self.itp.next()
            if event == "start":
                if node.tag == "results":
                    self.root = node
                if node.tag == "result":
                    yield node_to_dict(node)
                    self.root.remove(node)

if len(sys.argv) < 2:
    print "Usage: python api_response_csv.py server_response.xml"

else:
    try:
        f = open(sys.argv[1], "r")
    except IOError:
        print "File "+sys.argv[1]+" does not exist!"
        sys.exit(1)
        
    if sys.argv[1].endswith(".xml"):
        outname = sys.argv[1][:-len(".xml")]
    else:
        outname = sys.argv[1]

    out = csv.DictWriter(open(outname+".csv", "w"), ["id", "ErrorCount", "Errors"])
    
    for result in Results(f):
        if result and "ErrorCount" in result:
            if result["ErrorCount"] and result["ErrorCount"] != "0":
            #if result["ErrorCount"] == "1":
                print result
                out.writerow(result)
            