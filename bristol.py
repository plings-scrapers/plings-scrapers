#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import sys
import os

def pad(i):
    if len(i) == 1: return "0"+i
    else: return i

baseurl = "http://www.goplacesdothings.org.uk"
cachedir = "cache/bristol/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    next = None
    for month in ["01", "02", "03", "04"]:
        for  i in range(1,32):
            if i == 9 and month == "04":
                break
            if next:
                url = next
            else:
                url = baseurl + "/events/date/2011-"+month+"-" + pad(str(i))
            print url
            html = urllib2.urlopen(url).read()
            page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            for a in page1.findAll(attrs={"class": "see_all_tab read_more_orange_tab"}):
                print a["href"]
                m = re.match("/events/([A-Za-z0-9\-]+)", a["href"])
                id = m.group(1)
                if not id+".html" in os.listdir(cachedir):
                    print id
                    f = open(os.path.join(cachedir, id+".html"), "w")
                    f.write(urllib2.urlopen(baseurl + a["href"]).read())
                    f.close()

elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "bristol.xml"
    
    j=0
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        
        print id
        html = open(os.path.join(cachedir, file)).read()
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        activity = activities.create_activity()
        activity.add_field("ActivitySourceID", id)
        
        try:
            activity.add_field("Name", page.find(attrs={"class":"full_page_container_head"}).find("h1").contents[0])
            
            info = page.find(attrs={"class":"single_event_content_grey"})
            desc = ""
            for i in info.findAll(["div", "p"]):
                try:
                    if i["class"] == "white_details_box": continue
                except: pass
                
                try:
                    field = i.b.contents[0].strip()
                    try:
                        value = i.p.contents[0]
                    except AttributeError:
                        value = i.contents[1]
                    print field, value
                except AttributeError: continue
                except TypeError: continue
                except IndexError: continue
                
                if field == "Event date:":
                    try:
                        datestuff = value.split(" - ")
                        d = dateutil.parser.parse(datestuff[0]).date()
                        activity.add_field("Starts", datetime.datetime.combine(d, dateutil.parser.parse(datestuff[1]).time()).isoformat())
                        activity.add_field("Ends", datetime.datetime.combine(d, dateutil.parser.parse(datestuff[2]).time()).isoformat())
                    except IndexError: continue
                    except ValueError: continue
                elif field == "Type":
                    activity.keywords.add_field("Keywords", value)
                elif field == "Organiser":
                    pass
                elif field == "Venue:":
                    venstuff = value.split(",")
                    activity.venue.add_field("Name", venstuff[0])
                    activity.venue.add_field("ProviderVenueID", activity.hash(venstuff[0]))
                elif field == "Age range:":
                    ages = value.split(" to ")
                    activity.add_field("MinAge", ages[0])
                    activity.add_field("MaxAge", ages[1])
                elif field == "Postcode:":
                    activity.venue.add_field("Postcode", value.contents[0])
                elif field == "Description:":
                    if value:
                        desc = value
                elif field == "Contact Details:":
                    contactstuff = unicode(value).split("on")
                    if len(contactstuff) == 1: contactstuff = unicode(value).split("-")
                    activity.add_field("ContactName", contactstuff[0])
                    if len(contactstuff) > 1:
                        activity.add_field("ContactNumber", contactstuff[1])
            
            desc += '\n\n<a href="'+baseurl+'/events/'+id+'">View this event on Bristol\'s website</a>'
            activity.add_field("Description", desc)
            activity.finish()
        except: pass
            
        print
        print
        
    activities.finish()
