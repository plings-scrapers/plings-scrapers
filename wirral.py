#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup, urllib2, mechanize
from plings_sax import Activities
import datetime, dateutil.parser
import re, sys, os

def getpage(vurl, url2):
    if os.access(cachedir+vurl.encode("utf8")+".html", os.R_OK):
        html = open(cachedir+vurl.encode("utf8")+".html").read()
    else:
        html = urllib2.urlopen(url2.encode("utf8")).read()
        f = open(cachedir+vurl.encode("utf8")+".html", "w")
        f.write(html)
        f.close()
    return BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)

cachedir = "cache/wirral/"
baseurl = "http://www.teenwirral.com"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    br = mechanize.Browser()
    response = br.open(baseurl + "/whats-on/")
    next_day_link = None
    firstPage = True
    i = 0
    while True:
        print response.geturl()
        html = response.read()
        page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        
        for act in page1.findAll("div", "views-row"):
            a = act.find("a")
            url2 = baseurl + a["href"]
            print url2
            id = a["href"].replace("/activity-details/", "")
            if not (id+".html").encode("utf8") in os.listdir(cachedir):
                html = urllib2.urlopen(url2.encode("utf8")).read()
                print id
                f = open(os.path.join(cachedir, id.encode("utf8")+".html"), "w")
                f.write(html)
                f.close()
       
        i += 1 
        if i > 100:
            break        
        
        if firstPage:
            next_day_link = br.find_link(text_regex="Next")
            firstPage = False
        try:
            response = br.follow_link(text_regex="next")
        except mechanize._mechanize.LinkNotFoundError:
            response = br.follow_link(next_day_link)
            firstPage = True
        print
            
            
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "wirral.xml"
    
    for file in os.listdir(cachedir):
        try:
            file = file.decode("utf8")
            if file.endswith(".html"):
                id = file[:-len(".html")]
            else: continue
            
            print id
            html = open(os.path.join(cachedir,file)).read()
            page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            activity.add_field("Name",
                page.find("h2", "with-tabs").contents[0] )
            activity.add_field("MinAge",
                page.find("div", "field-field-activity-age-from").find("div", "field-item odd").contents[2].strip() )
            activity.add_field("MaxAge",
                page.find("div", "field-field-avtivity-age-to").find("div", "field-item odd").contents[2].strip() )
            activity.add_field("Description",
                unicode(page.find("div", "content clear-block").find("p").contents[0]) + "<br/>\n<br/>\nFor more information see <a href=\"http://www.teenwirral.com/activity-details/"+id+"\">this event on the Teen Wirral website</a>." )
            activity.add_field("Cost",
                page.find("div", "field-field-activity-cost").find("div", "field-item odd").contents[0].strip() )
            activity.add_field("ContactName",
                page.find("div", "field-field-activity-contact-name").find("div", "field-item odd").contents[0].strip() )
            activity.add_field("ContactNumber",
                page.find("div", "field-field-activity-contact-number").find("div", "field-item odd").contents[0].strip() )
            
            vurl = page.find("div", "field-field-activity-location").find("div", "field-item odd").a["href"]
            url2 = baseurl + vurl
            vpage = getpage(vurl, url2)
            
            activity.venue.add_field("ProviderVenueID", vurl.replace("activity-location", ""))
            activity.venue.add_field("Name", vpage.find(id="squeeze").h2.contents[0])
            addr = vpage.find("div", "field-field-loc-address").find("div", "field-item odd").contents[0]
            addr = unicode(addr).strip()
            m = re.match("[0-9]+", addr)
            if m:
                activity.venue.add_field("BuildingNameNo", m.group(0))
            m = re.search("[A-Za-z0-9]{3,4} [A-Za-z0-9]{3,4}$", addr)
            if m:
                activity.venue.add_field("Postcode", m.group(0))
            else: continue
            activity.venue.add_field("CyclePark",
                vpage.find("div", "field-field-loc-cycle-park").find("div", "field-item odd").contents[0] )
            # Disabled information?
            
            """
            purl = page.find("div", "field-field-run-by").find("div", "field-item odd").a["href"]
            url2 = baseurl + purl
            if os.access(cachedir+purl.encode("utf8")+".html", os.R_OK):
                html = open(cachedir+purl.encode("utf8")+".html").read()
            else:
                html = urllib2.urlopen(url2.encode("utf8")).read()
                f = open(cachedir+purl.encode("utf8")+".html", "w")
                f.write(html)
                f.close()
            ppage = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
                        
            activity.venue.add_field("DPProviderID", vurl.replace("provider", ""))
            """
            
            repurl = page.find("a", text="Repeats").parent["href"]
            reppage = getpage("repeats/"+re.search("[0-9]+",repurl).group(0), baseurl+repurl)
            #date = page.find("div", "field-field-activity-start-date").find("div", "field-item odd")
            print reppage.find("div","field-field-activity-start-date")
            for daterow in reppage.find("div","field-field-activity-start-date").findAll("div","field-item"):
                date = dateutil.parser.parse( daterow.find("span", "date-display-single").contents[0] ).date()
                if date < datetime.date.today(): continue
                starts = dateutil.parser.parse( daterow.find("span", "date-display-start").contents[0] ).time()
                ends = dateutil.parser.parse( daterow.find("span", "date-display-end").contents[0] ).time()
                
                activity2 = activities.cloneactivity(activity, activity.id+"-"+date.isoformat())
                activity2.add_field("Starts", datetime.datetime.combine(date, starts).isoformat())
                activity2.add_field("Ends", datetime.datetime.combine(date, ends).isoformat())
                activity2.finish()
            
            
            print
        except DeprecationWarning:
            pass
        except AttributeError:
            pass
        
    activities.finish()
        
