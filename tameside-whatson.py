#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re


activities = Activities()
activities.outname = "tameside.xml"

baseurl = "http://whatson.tameside.gov.uk/"
url = "http://whatson.tameside.gov.uk/?searchMode=List&LookAheadDays=1000"
html = urllib2.urlopen(url).read()
page1 = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)

i=0
for h4 in page1.findAll("h4"):
    try:
        print h4.a["href"]
        m = re.match("/Event/\w+/([0-9]+)", h4.a["href"])
        id = m.group(1)
        #m = re.match(".*\?DateText=(.*)")
        
        url = baseurl+h4.a["href"]
        html = urllib2.urlopen(url).read()
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        activity = activities.create_activity()
        details = page.find(attrs={"id":"event_details"})
        
        dtinfo = "".join(details.h2.findAll(text=True)).split(" from ")
        print dtinfo
        d = dateutil.parser.parse(dtinfo[0]).date()
        tinfo = dtinfo[1].split(" to ")
        activity.add_field("Starts", datetime.datetime.combine(d, dateutil.parser.parse(tinfo[0]).time()).strftime("%Y-%m-%dT%H:%M:%S"))
        activity.add_field("Ends", datetime.datetime.combine(d, dateutil.parser.parse(tinfo[1]).time()).strftime("%Y-%m-%dT%H:%M:%S"))
        
        activity.add_field("ActivitySourceID", id+"-"+d.strftime("%Y%m%d"))
        activity.add_field("Name", details.h1.a.contents[0])
        
        desc = "".join(details.find(attrs={"class":"description"}).findAll(text=True))
        print desc
        activity.add_field("Description", desc)
        
        m = re.match(".*?([A-Z0-9]+ [A-Z0-9]+)", details.find(attrs={"class":"address_display"}).div.contents[0])
        activity.venue.add_field("Postcode", m.group(1))
        venue_name = details.h3.contents[0]
        activity.venue.add_field("Name", venue_name)
        activity.venue.add_field("ProviderVenueID", activity.hash(venue_name))
        for p in details.findAll("p"):
            m = re.match("Contact phone number: (.*)", str(p.contents[0]))
            if m:
                activity.add_field("ContactNumber", m.group(1))
        activity.finish()
    except:
        pass

activities.finish()