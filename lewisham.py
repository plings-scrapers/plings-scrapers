#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import csv
from plings import Activities, day2int
import datetime
import re
import dateutil.parser

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "utf-8") for cell in row]

def numtrim(num):
    m = re.match("[0-9]+", num)
    if m:
        return m.group(0)
    else:
        return ""

def day2intwrapper(day):
    m = re.search("[A-Za-z]+", day)
    if m:
        day = m.group(0)
        return day2int(day)
    else:
        raise ValueError("No characters!")

spread = unicode_csv_reader(open("Plings-MASTER-Spreadsheet-16thJuly.csv"))
activities = Activities()
#header = spread.next()
activities.outname = "lewisham.xml"

flipped = []
for row in spread:
    i=0
    for cell in row:
        if i >= len(flipped):
            flipped.append([])
        flipped[i].append(cell)
        i+=1
        
header1 = flipped.pop(0)
header2 = flipped.pop(0)

for row in flipped:
    id, title, description, cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, \
        addinfo, booking, referral, waiting_list, cpp, \
        startdate, enddate, day, freq, starttime, endtime, \
        orgname, orgtype1, orgtype2, orgtype3, orgtype4, orgtype5, \
        contactname, contactnumber, contactemail, contactaddr, minage, maxage, cost, \
        keywords, ecm, pm, pv, cca, \
        venid, venname, buildingnameno, postcode, \
        vencontactforename, vencontactsurname, vencontactrole, vencontactnumber, vencontactmobile, vencontactemail, vencontactfax, \
        vendescription, website, parking, cyclepark, disablednotes, dda1, dda2, dda3 = row[:59]
    
    activity = activities.create_activity()
    activity.add_field("ActivitySourceID", activity.hash(title))
    activity.add_field("Name", title)
    activity.add_field("Description", description+"\n \n"+addinfo)
    activity.add_field("ContactName", contactname)
    activity.add_field("ContactNumber", contactnumber)
    activity.add_field("ContactEmail", contactemail)
    activity.add_field("ContactAddress", contactaddr)
    activity.add_field("MinAge", numtrim(minage))
    activity.add_field("MaxAge", numtrim(maxage))
    activity.add_field("Cost", cost)
    for keyword in keywords.split(","):
        activity.keywords.add_field("Keyword", keyword)
    
    activity.venue.add_field("ProviderVenueID", activity.hash(venname))
    activity.venue.add_field("Name", venname)
    activity.venue.add_field("BuildingNameNo", buildingnameno)
    activity.venue.add_field("Postcode", postcode)
    activity.venue.add_field("ContactForename", vencontactforename)
    activity.venue.add_field("ContactSurname", vencontactsurname)
    activity.venue.add_field("ContactPhone", vencontactnumber)
    activity.venue.add_field("ContactEmail", vencontactemail)
    activity.venue.add_field("ContactFax", vencontactfax)
    activity.venue.add_field("Description", vendescription)
    
    if orgname:
        activity.add_org()
        activity.org.add_field("DPProviderID", activity.hash(orgname))
        activity.org.add_field("Name", orgname)
    
    try: d1 = dateutil.parser.parse(startdate).date()
    except ValueError: d1 = None
    try: d2 = dateutil.parser.parse(enddate).date()
    except ValueError: d2 = None
    #print d1, d2
    
    try:
        time = [dateutil.parser.parse(starttime.strip()).time(), dateutil.parser.parse(endtime.strip()).time()]
    except ValueError as e:
        print "time:", e
        continue
    
    #print day
    try:
        days = re.split(u"\-|to|\u2013", day)
        if len(days) > 1:
            di = map(day2intwrapper, days)
            #print di
            if di[0] < di[1]:
                days = range(di[0], di[1]+1)
            else:
                raise Exception("Day sequence over week boundaries not implemented.")
        else:
            days = re.split(u"\&|\,|and", day)
            days = map(day2intwrapper, days)
        for dayint in days:
            activity2 = activities.cloneactivity(activity, activity.id+unicode(dayint))
            activity2.setup_recurring(dayint, time, d1, d2)
            activity2.finish()
            activity2.recur()
    except ValueError as e:
        print u"day:", unicode(e)
        #import traceback, sys
        #print traceback.print_exc(file=sys.stdout)
        continue
    #activity.finish()
    #break

activities.finish()
#print activities.doc.toprettyxml(indent="    ")
    