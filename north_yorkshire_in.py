#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import csv
from plings import Activities
import datetime, dateutil.parser

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "utf-8") for cell in row]

spread = unicode_csv_reader(open("north_yorkshire.csv", "r"))
activities = Activities()
activities.outname = "north_yorkshire.xml"

for row in spread:
    try:
        id, name, venue, nameagain, address, time, cost, disabled, contact, contactemail, contactphone = row
    except ValueError:
        try:
            id, name, venue, nameagain, address, time, cost, disabled, contact, contactemail = row
            contactphone = ""
        except ValueError: pass
    activity = activities.create_activity()
    dt = dateutil.parser.parse(time)
    activity.add_field("ActivitySourceID", id+"-"+dt.strftime("%Y%m%d"))
    activity.add_field("Name", name)
    activity.add_field("Description", nameagain+" \nEnd time is approximate. \nSee this activity on <a href=\"http://www.gimi.co.uk/index.aspx?articleid="+id+"\">Gimi</a>")
    activity.venue.add_field("ProviderVenueID", activity.hash(venue.split(",")[0]))
    activity.venue.add_field("Name", venue.split(",")[0])
    activity.venue.add_field("Postcode", address.split(",")[0])
    activity.add_field("Starts", dt.isoformat())
    activity.add_field("Ends", (dt+datetime.timedelta(hours=2)).isoformat())
    activity.add_field("Cost", cost.strip(u"\xa3 "))
    activity.venue.add_field("DisableFacilitiesNotes", "Disabled access? "+disabled)
    activity.add_field("ContactName", contact)
    activity.add_field("ContactEmail", contactemail)
    activity.add_field("ContactNumber", contactphone)
    activity.finish()
    
activities.finish()
