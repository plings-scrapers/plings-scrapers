#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import pickle
import sys
import re
from suds.client import Client
import config

try:
    id = sys.argv[1]
except:
    print "Please specify an ID."
    sys.exit()

data = pickle.load(open("bbcblast_in.py.soap_pickle"))
client = Client("http://feeds.plings.net/services/activities.php?wsdl", cache=None)
r =  client.service.register(config.keys["bbcblast"]+"-L")
if r == 0:
    raise Exception("Registration with the API failed, check your API key")

for (k,v) in data.items():
    m = re.match("bbcblast-([0-9]+)-([0-9]+)", k)
    if m.group(1) == id:
        print v
        r = client.service.deleteActivity(v)
        print r
        if r or True:
            del data[k]

pickle.dump(data, open("bbcblast_in.py.soap_pickle", "w"))
