#!/usr/bin/env python
# Copyright (c) 2010, 2011 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details
import sys, os
from plings_sax import Activities

if len(sys.argv) < 3:
    print "Usage python scrape.py scrapername [get|scrape|full]"
    exit(0)

name = sys.argv[1]
name = name.replace(".py", "")

s = __import__(name)
scraper = s.Scraper()
if not scraper.name: scraper.name = name

if sys.argv[2] == "get":
    scraper.get()
    
elif sys.argv[2] == "scrape":
    scraper.scrape()
    
elif sys.argv[2] == "full":
    try:
        import shutil
        shutil.rmtree(name)
    except OSError: pass
    scraper.get()
    scraper.scrape()
    import split, xml_post
    max = split.split(name+".xml")
    for i in range(1,max+1):
        print i
        xml_post.post(name+"_"+str(i)+".xml", name)

else:
    print "Usage python scrape.py scrapername [get|scrape|full]"
