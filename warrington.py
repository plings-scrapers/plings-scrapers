#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details
import sys, os
from plings_sax import Activities
import BeautifulSoup, urllib2, mechanize
import re, datetime, dateutil.parser
import pickle

def mapmagic(url):
    global br
    try:
        f = open("mapmagic.pickle", "r")
        locations = pickle.load(f)
        f.close()
    except IOError:
        locations = {}
    if not url in locations:
        try:
            br.set_handle_robots(False)
            html = br.open(url).read()
            m1 = re.search('markers:[{id:"A",cid:"[0-9]*",latlng:{lat:(.*?),lng:(.*?)}', html)
            print m1.group(0)
            m2 = re.search('sxti:"(.*?)",sxst:".*?",sxsn:".*?",sxct:".*?",sxpr:".*?",sxpo:"(.*?)",sxcn:"GB",sxph:"(.*?)"', html)
            print m2.group(0)
            locations[url] = [ m1.group(1), m1.group(2), m2.group(1), m2.group(2), m2.group(3) ]
        except AttributeError: locations[url] = "FAIL"
        f = open("mapmagic.pickle", "w")
        pickle.dump(locations, f)
        f.close()
    return locations[url]
    
    

cachedir = "cache/warrington/"
baseurl = "http://www.wot2do.org/"

br = mechanize.Browser()

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    for month in range(datetime.date.today().month, (datetime.date.today()+datetime.timedelta(weeks=15)).month):
        br.open(baseurl+"calendar/?year=2011&month="+str(month))
        html = br.response().read()
        page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        select = page.find("select", "fsize")
        for option in select.findAll("option"):
            cid = option["value"]
            br.open("http://www.wot2do.org/calendar/?year=2011&month="+str(month)+"&cid="+cid)
            html = br.response().read()
            
            for (url, id, time) in re.findall("../(activity/\?id=([0-9]+)&time=([0-9]+))", html):
                url = baseurl + url
                print url
                id = id + "-" + time
                if not id.encode("utf8")+".html" in os.listdir(cachedir):
                    html = urllib2.urlopen(url.encode("utf8")).read()
                    print id
                    f = open(os.path.join(cachedir, id.encode("utf8")+".html"), "w")
                    f.write(html)
                    f.close()
    
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "warrington.xml"
    
    for file in os.listdir(cachedir):
        try:
            file = file.decode("utf8")
            if file.endswith(".html"):
                id = file[:-len(".html")]
            
            #print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            timestamp = re.sub(".*-", "", id)
            activity.add_field("Starts",
                datetime.datetime.fromtimestamp(float(timestamp)).isoformat() )
            activity.add_field("Ends",
                datetime.datetime.fromtimestamp(float(timestamp)+60.0*60.0).isoformat() )
            
            for strong in page.findAll("strong"):
                if strong.div:
                    activity.add_field("Name", strong.div.contents[0])
                #if strong.contents[0] == "Location:":
                    #print strong.next.next
                #elif strong.contents[0] == "Age:":
                    #print strong.next.next
                #elif strong.contents[0] == "Cost:":
                    #print strong.next.next 
                #elif strong.contents[0] == "Contact:":
                    #activity.add_field("ContactName", strong.next.next)
                elif strong.contents[0] == "Info:":
                    activity.add_field("Description", strong.next.next.next.next)
            
            loc = page.find(text="Location:")
            print loc.parent.nextSibling
            
            try:
                mapurl = page.find("iframe")["src"]
                location = mapmagic(mapurl)
            except TypeError: continue
            if location == "FAIL": continue
            
            activity.venue.add_field("ProviderVenueID", activity.hash(mapurl))
            activity.venue.add_field("GeoCoordSystem", "WGS84DD")
            activity.venue.add_field("GeoCoordX", location[0])
            activity.venue.add_field("GeoCoordY", location[1])
            activity.venue.add_field("Name", location[2])
            activity.venue.add_field("Postcode", location[3]) 
            activity.venue.add_field("ContactPhone", location[4])
            
            activity.finish()
        
        except AttributeError, e: print "AttributeError", e
        
    activities.finish()