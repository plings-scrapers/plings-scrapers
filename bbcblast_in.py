#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings_soap import Activities
import csv
import re
import datetime
import config
import sys

def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "utf-8") for cell in row]

def decode_date(date):
    date = date.strip()
    return datetime.datetime.strptime(date, "%d/%m/%y").date()

def decode_time(time):
    time = time.strip()
    return datetime.datetime.strptime(time, "%H:%M:%S").time()

try:
    id = sys.argv[1]
except:
    print "Please specify an ID."
    sys.exit()

spread = unicode_csv_reader(open("bbcblast_"+id+".csv", "r"))
#head = spread.next()

activities = Activities()
activities.register(config.keys["bbcblast"]+"-L")

#idstem = "Liverpool201005"
idstem = "bbcblast-"+id

i=0
previd = 0
firstid = 0
for date, starts, ends, title, description, notes, info, venueid, orgid, keyword in spread:
    i+=1
    activity = activities.create_activity()
    activity.disable_venue = True
    activity.add_field("ActivitySourceID", idstem+"-"+str(i))
    activity.add_field("Name", title)
    activity.add_field("Description", description+"\n\n"+info)
    d = decode_date(date)
    activity.add_field("Starts", datetime.datetime.combine(d, decode_time(starts)))
    activity.add_field("Ends", datetime.datetime.combine(d, decode_time(ends)))
    activity.add_field("VenueID", venueid)
    activity.keywords.add_field("Keyword", keyword)
    activity.taxonomy.add_field("bbcblastnotes", notes.strip())
    previd = activity.finish()
    if firstid > 0:
        activity.add_field("LinkWithActivity", firstid)
    else:
        firstid = previd
    
activities.finish()
