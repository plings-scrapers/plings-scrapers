#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import hashlib
import datetime
import csv
import sys
import re
import os

cachedir = "cache/nafis/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":   
    for url in ["http://nafis.co.uk/index.php?option=com_comprofiler&task=userslist&listid=2&Itemid=73",
                "http://nafis.co.uk/index.php?option=com_comprofiler&task=usersList&listid=2&Itemid=73&limitstart=154"]:
        html = urllib2.urlopen(url).read()
        page1 = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        for td in page1.findAll(attrs={"class": "cbUserListCol1"}):
            a = td.find("a")
            m = re.match(".*user=([0-9]+)", a["href"])
            id = m.group(1)
            if not id+".html" in os.listdir(cachedir):
                html = urllib2.urlopen(a["href"]).read()
                f = open(cachedir+id+".html", "w")
                f.write(html)
                f.close()

elif sys.argv[1] == "scrape":
    out = csv.writer(open("nafis.csv", "w"))
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        
        print id
        html = open(os.path.join(cachedir, file)).read()
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        row = []
        row.append(id)
        for td in page.findAll(attrs={"class":"fieldCell"}):
            cell = "".join(td.findAll(text=True)).encode("utf-8")
            row.append(cell)
        out.writerow(row)
        