#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import sys
import os

activities = Activities()
activities.outname = "east_riding.xml"

baseurl = "http://events.eastriding.gov.uk/"
cachedir = "cache/east_riding/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    import pickle
    file = open("windmill/east_riding.pickle", "r")
    links = pickle.load(file)
    for link in links:
            m = re.search("id=([0-9]+)", link)
            id = m.group(1)
            if not id+".html" in os.listdir(cachedir):
                print id
                f = open(cachedir+id+".html", "w")
                f.write(urllib2.urlopen(baseurl+link).read())
                f.close()

elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "east_riding.xml"
    
    j=0
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        try:
            print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            activity.add_field("Name", page.find(id="ctl00_lblPageTitle").contents[0])
            activity.add_field("Description", page.find(id="ctl00_cphMaster_lblDescription").contents[0])
            try: activity.add_field("ContactName", page.find(id="ctl00_cphMaster_lblContact").contents[0])
            except: pass
            try: activity.add_field("ContactNumber", page.find(id="ctl00_cphMaster_lblTelephone").contents[0])
            except: pass
            try: activity.add_field("ContactEmail", page.find(id="ctl00_cphMaster_lnkEmail").contents[0])
            except: pass
            #print ctl00_cphMaster_lblDates ctl00_cphMaster_lblTimes
            date = page.find(id="ctl00_cphMaster_lblDates").contents[0]
            times = page.find(id="ctl00_cphMaster_lblTimes").contents[0].split(" - ")
            s = date+" "+times[0]
            e = date+" "+times[1]
            activity.add_field("Starts", dateutil.parser.parse(s).isoformat())
            activity.add_field("Ends", dateutil.parser.parse(e).isoformat())
            
            venue_name = unicode(page.find(id="ctl00_cphMaster_lblHouse").contents[0])
            activity.venue.add_field("ProviderVenueID", activity.hash(venue_name))
            activity.venue.add_field("Name", venue_name)
            activity.venue.add_field("Postcode", page.find(id="ctl00_cphMaster_lblPostcode").contents[0])
            
            activity.finish()
        except: continue
        
    activities.finish()
    print activities.doc.toprettyxml(indent="    ")

