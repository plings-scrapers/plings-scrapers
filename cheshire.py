#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
from plings_sax import Activities
import datetime, dateutil.parser
import re, sys, os
import mechanize

cachedir = "cache/cheshire/"
baseurl = "http://www.13plus.stuff2doincheshire.co.uk"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    br = mechanize.Browser()
    d = datetime.date.today()
    for n in range(1,5):
        date = "1/" + str(n) + "/2011"
        url = baseurl + "/whatson/?date=" + date
        i = 1
        while True:
            id = date.replace("/","-") + "_" + str(i)
            print id, url
            if id+".html" in os.listdir(os.path.join(cachedir, "tmp")):
                html = open(os.path.join(cachedir, "tmp", id+".html")).read()
            else:
                f = open(os.path.join(cachedir, "tmp", id+".html"), "w")
                html = br.open(url).read()
                f.write(html)
                f.close()
            page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            for h2 in page1.findAll("h2"):
                if h2.a:
                    url2 = baseurl + h2.a["href"]
                    m = re.search("id=([0-9]*)", url2)
                    id2 = m.group(1)
                    print " ", id2, url2
                    if not id2+".html" in os.listdir(os.path.join(cachedir, "activities")):
                        html = br.open(url2).read()
                        f = open(os.path.join(cachedir, "activities", id2+".html"), "w")
                        f.write(html)
                        f.close()
            
            try:
                url = baseurl + "/whatson/" + page1.find("a", text=">").parent["href"]
            except KeyError:
                break
            d += datetime.timedelta(days=1)
            i +=1
                
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "cheshire.xml"
    
    for file in os.listdir(os.path.join(cachedir,"activities")):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        
        try:
            html = open(os.path.join(cachedir, "activities", file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            event = page.find(id="event")
            desc = unicode(event.find("div", "summary"))
            dlist = []
            print "    " + id
            activity = activities.create_activity()
            details = event.find("ul", "event-details")
            for li in details.findAll("li"):
                if li.contents[0].name == "a": startfrom = 1
                else: startfrom = 0
                k = li.contents[startfrom].contents[0]
                v = " ".join(map(unicode, li.contents[startfrom+1].contents))
                if k == "Forthcoming Dates" or k == "Other Future Dates":
                    if k == "Other Future Dates": dates = li.contents[startfrom+1].contents
                    else: dates = li.contents[startfrom+1:]
                    for date in dates:
                        try:
                            dlist.append( dateutil.parser.parse(date.find(text=True)).date() )
                        except ValueError:
                            print "Bad date"
                        except TypeError:
                            print "Weird date"
                elif k == "Age Range":
                    ages = re.findall("[0-9]{1,2}", v)
                    if len(ages) >= 2:
                        activity.add_field("MinAge", ages[0])
                        activity.add_field("MaxAge", ages[1])
                elif k == "Cost for non-members":
                    activity.add_field("Cost", v)
                elif k == "Cost for members":
                    pass
                else:
                    desc += "<p>" + k + ": " + v + "</p>"
                    
            times = []
            for h2 in event.findAll("h2"):
                k = h2.contents[0]
                v = " ".join(map(unicode, h2.nextSibling.contents))
                if k == "Opening Hours:":
                    times = re.findall("[0-9\:\.APMapm]{3,}", v)
                else:
                    desc += "<p>" + k + ": " + v + "</p>"                
            
            activity.add_field("ActivitySourceID", id)
            activity.add_field("Name", page.find("h1").contents[0])
            desc += "<p><a href=\"" + baseurl + "/whatson/event/?id=" + id + "\">See this event on Cheshire's website.</a></p>"
            activity.add_field("Description", desc)
            
            venue = page.find("div", "venue-details")
            activity.venue.add_field("ProviderVenueID", activity.hash(venue.h2.contents[0]))
            activity.venue.add_field("Name", venue.h2.contents[0])
            addr = venue.find("address").contents[0].split(",")
            activity.venue.add_field("Postcode", addr[-1])
            m = re.search("[0-9]+", addr[0])
            if m:
                activity.venue.add_field("BuildingNameNo", m.group(0))
            activity.venue.add_field("ContactNumber", venue.find("dd").contents[0])
            activity.venue.add_field("Website", baseurl + venue.find("img", alt="More Information").parent["href"].strip())
            
            
            try:
                for d in dlist:
                    activity2 = activities.cloneactivity(activity, id+"_"+d.isoformat())
                    if times[0][-1] != "m":
                        if times[1][-1] == "m":
                            times[0] += times[1][-2:]
                        else:
                            times[0] += "pm"
                    if dateutil.parser.parse(times[0]).time() == datetime.time(0,0,0):
                        print times[0]
                    activity2.add_field("Starts", datetime.datetime.combine(d,dateutil.parser.parse(times[0]).time()).isoformat())
                    activity2.add_field("Ends", datetime.datetime.combine(d,dateutil.parser.parse(times[1]).time()).isoformat())
                    activity2.finish()
            except IndexError:
                print "Insufficient times"
            except ValueError:
                print "Bad times"
            except TypeError:
                print "Weird times"
        except:
            "ERROR"
    
    activities.finish()
