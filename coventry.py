#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from plings import Activities
import urllib, urllib2
import BeautifulSoup
import sys, os
import re, dateutil.parser

def timeparse(time):
    return dateutil.parser.parse(time).time()

baseurl = "http://activities.coventry.gov.uk/"
cachedir = "cache/coventry/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    url = baseurl+"SearchbyCategory.aspx"
    values = { "CategoryDropDownList": "-99",
            "__VIEWSTATE" : "dDwxMjE0MDEzODE2O3Q8O2w8aTw1Pjs+O2w8dDw7bDxpPDM+O2k8NT47PjtsPHQ8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDxjYXRfTmFtZTtjYXRfSUQ7Pj47Pjt0PGk8ODA+O0A8UGxlYXNlIFNlbGVjdCBhbiBBY3Rpdml0eTtBbGwgQWN0aXZpdGllczs1MCsgQWN0aXZpdGllcztBZXJvYmljcztBaWtpZG87QW5nbGluZztBcmNoZXJ5DQo7QXRobGV0aWNzO0JhZG1pbnRvbg0KO0Jhc2tldGJhbGw7QmVsbHkgRGFuY2luZztCb2F0IFJhY2luZztCb2R5IFB1bXA7Qm93bHM7Qm94IEZpdDtCb3hpbmc7QnJlYWtkYW5jaW5nO0J1bXMsIFR1bXMgJiBUaGlnaHM7Q2Fub2Vpbmc7Q2hlZXJsZWFkaW5nO0NpcmN1aXQgVHJhaW5pbmc7Q2xpbWJpbmc7Q29tYmF0O0NyYWZ0cztDcmVhdGl2ZSBXcml0aW5nO0NyaWNrZXQ7Q3JvcXVldDtDeWNsaW5nIA0KO0RhbmNlDQo7RHJhbWE7RmVuY2luZztGaXQgQmFsbDtGb290YmFsbDtHby1LYXJ0aW5nO0dvbGY7R3ltIFdvcmtvdXQ7R3ltbmFzdGljcyANCjtIb2NrZXk7SG9yc2UgUmlkaW5nO0ljZSBIb2NrZXkgDQo7SWNlIFNrYXRpbmcgDQo7SnVkbztLYXJhdGU7S2F5YWtpbmc7S2ljayBCb3hpbmc7TWFydGlhbCBBcnRzO01vZGVsIEFpcmNyYWZ0IA0KO011c2ljO05ldGJhbGw7T3RoZXJzO1BldGFucXVlO1Bob3RvZ3JhcGh5O1BpbGF0ZXM7UmFtYmxpbmc7UmlmbGUgDQo7UnVnYnkgDQo7U2FpbGluZztTYWxzYTtTY3ViYSBEaXZpbmc7U2hvb3Rpbmc7U2luZ2luZztTa2lpbmc7U25vb2tlciANCjtTcG9ydCBBY3Rpdml0eSBTZXNzaW9ucztTcXVhc2g7U3RlcDtTd2ltbWluZztUYWJsZSBUZW5uaXM7VGFpIENoaTtUZW5uaXMNCjtUcmFtcG9saW5pbmc7VW5kZXIgNTtWb2xsZXliYWxsO1ZvbHVudGVlcmluZztXYWxraW5nO1dhdGVyIFBvbG87V2VpZ2h0IFRyYWluaW5nO1doZWVsY2hhaXIgQmFza2V0YmFsbDtXcmVzdGxpbmc7WW9nYTs+O0A8UGxlYXNlIFNlbGVjdCBhbiBBY3Rpdml0eTstOTk7ODE7MjszOzQ7NTs2Ozc7ODs0OTs1Mzs1NDs5OzYzOzEwOzc5OzYwOzExOzU4OzU1OzUyOzU2Ozc1Ozc4OzEyOzEzOzE0OzE1OzcwOzcxOzY0OzE2OzY4OzQ3OzY5OzE3OzE4OzcyOzE5OzIwOzgwOzIxOzIyOzU3OzIzOzI0Ozc0OzI1OzQ1Ozg0Ozc3OzUxOzI3OzI4OzI5OzMwOzMxOzMyOzczOzY2OzY1OzMzOzY3OzM0OzYyOzM1OzM2OzM3OzM4OzM5OzUwOzQwOzgzOzQ4OzQxOzYxOzgyOzQzOzQ0Oz4+O2w8aTwyPjs+Pjs7Pjt0PHA8cDxsPFRleHQ7VmlzaWJsZTs+O2w8Tm8gQWN0aXZpdGllcyBGb3VuZDtvPHQ+Oz4+Oz47Oz47Pj47Pj47PlLU8gQWbx9DDUKMk5oqRKS4tlkQ"
        }
    
    data = urllib.urlencode(values)
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    html = response.read()
    
    page = BeautifulSoup.BeautifulSoup(html)
    
    #print page;
    table = page.find("table")
    trs = table.findAll('tr')
    
    for tr in trs:
        try:
            link = tr.find("a")["href"]
            print link
            m = re.search("ses_ID=([0-9]+)", link)
            id = m.group(1)       
            if not id+".html" in os.listdir(cachedir):
                print id
                f = open(cachedir+id+".html", "w")
                f.write(urllib2.urlopen(baseurl+link).read())
                f.close()
        except TypeError: continue


elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "coventry_1.xml"
    
    j=0
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        try:
            print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            
            try: activity.keywords.add_field("Keyword", page.find(id="CategoryLabel").contents[0])
            except IndexError: pass
            except AttributeError: pass
            activity.add_field("Name", page.find(id="ActivityTitleLabel").contents[0])
            try:
                ages = page.find(id="AgeLabel").contents[0]
                m = re.search("From[ ]*([0-9]+)[ ]*to[ ]*([0-9]+)", ages)
                activity.add_field("MinAge", m.group(1))
                activity.add_field("MaxAge", m.group(2))
            except IndexError: pass
            except AttributeError: pass
            try: activity.add_field("Cost", page.find(id="PriceLabel").contents[0])
            except IndexError: pass
            try: activity.add_field("ContactName", page.find(id="ContactPersonLabel").contents[0])
            except IndexError: pass
            try: activity.add_field("ContactNumber", page.find(id="TelephoneLabel").contents[0])
            except IndexError: pass
            address = page.find(id="AddressLabel").contents[0].split(",")[0]
            activity.venue.add_field("ProviderVenueID", activity.hash(address))
            activity.venue.add_field("Name", address)
            activity.venue.add_field("Postcode", page.find(id="PostCodeLabel").contents[0])
            activity.add_field("Description", page.find(id="DescriptionLabel").contents[0])
            
            table = page.find(id="TimingsDataGrid")
            trs = table.findAll("tr")
            ths = trs[0].findAll("th")
            tds = trs[1].findAll("td")
            i=0
            for td in tds:
                if not td.contents[0].strip() == "":
                    day = ths[i].font.contents[0]
                    times = map(timeparse, td.contents[0].split("-"))
                    try:
                        activity2 = activities.cloneactivity(activity, id+"-"+day)
                        activity2.setup_recurring(day, times)
                        activity2.finish()
                        activity2.recur()
                        j+=1
                    except: continue
                i+=1
            
            if j==700:
                activities.finish()
                activities = Activities()
                activities.outname = "coventry_2.xml"
            
        except IndexError: continue
        except AttributeError: continue
    
    activities.finish()
    print j
    #print activities.doc.toprettyxml(indent="    ")
