#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details
import couchdb
from xml.etree.ElementTree import iterparse, tostring
import urllib, urllib2, mechanize
import copy, re

couch = couchdb.Server()
if "culture24test" in couch:
    db = couch["culture24test"]
else:
    db = couch.create("culture24test")

multiples = ["""venue", """ "nationalCurriculumTag", "contentTag", "audience", "instance" "language", "relatedEvent"
             "creator", "publisher"]

def node_to_dict(node):
    if len(node) == 0: return node.text
    #data = copy.copy(node.attrib)
    data = {}
    for child in node:
        childdata = node_to_dict(child)
        tag = re.sub("{.*}", "", child.tag)
        if tag in multiples:
            if not data.has_key(tag):
                data[tag] = []
            data[tag].append(childdata)
        else:
            data[tag] = childdata
    return data

headers = { "User-Agent" : "benapiaccesser" }
baseurl = "http://www.culture24.org.uk/api/oai/redacted?verb=ListRecords"
br = mechanize.Browser()
## Need to get a cookie from this page (which is why I'm doing stuff manually)
br.open("http://www.culture24.org.uk/sector+info/data+sharing/art74965")

for set in ["events"]:#"venues"]:
    url = baseurl + "&metadataPrefix=c24&set=" + set
    f = br.open(url)
    itp = iter(iterparse(f, events=['start', 'end']))
    docs = []
    while True:
        event, node = itp.next()
        if event == "start":
            if node.tag == "{http://www.openarchives.org/OAI/2.0/}ListRecords":
                root = node
        elif event == "end":
            if node.tag == "{http://www.openarchives.org/OAI/2.0/}record":
                tmp = node_to_dict(node)
                type = tmp["metadata"]["record"].keys()[0]
                data = tmp["metadata"]["record"][type]
                data["couchtype"] = type
                id = tmp["header"]["identifier"]
                data["_id"] = id
                print id
                if id in db:
                    #continue
                    data["_rev"] = db[id]["_rev"]
                docs.append(data)
                root.remove(node)
            elif node.tag == "{http://www.openarchives.org/OAI/2.0/}resumptionToken":
                print "Test"
                import time
                time.sleep(5)
                print tostring(node)
                print node.text
                f.close()
                f = None
                url = baseurl + "&resumptionToken=" + urllib.quote_plus(node.text)
                print url
                while True:
                    try:
                        f = br.open(url)
                        break
                    except urllib2.URLError:
                        print "URLError"
                        import time
                        time.sleep(30)
                itp = iter(iterparse(f, events=['start', 'end']))
                db.update(docs)
                docs = []
