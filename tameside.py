#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import sys
import os

def dateparse(date):
    date = date.replace(u"\xa0", u" ")
    return dateutil.parser.parse(date).date()

def deflist_to_dict(dl):
    data = {}
    key = ""
    for tag in dl.findAll(["dt", "dd"]):
        if tag.name == "dt":
            key = "".join(tag.findAll(text=True))
        elif tag.name == "dd":
            data[key] = "".join(tag.findAll(text=True))
    return data

cachedir = "cache/tameside/"
baseurl = "http://www.tameside-sid.org.uk/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    url = baseurl + "young.asp?catid=64"
    html = urllib2.urlopen(url).read()
    page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
    
    tab = page1.find(id="tab")
    for a in tab.findAll("a"):
        url2 = baseurl + "young.asp" + a["href"]
        print url2
        html = urllib2.urlopen(url2).read()
        page2 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        for a in page2.findAll("a", attrs={"class": "more"}):
            m = re.search("lid=([0-9]+)", a["href"])
            id = m.group(1)
            if not id+".html" in os.listdir(cachedir):
                print id
                f = open(os.path.join(cachedir, id+".html"), "w")
                f.write(urllib2.urlopen(baseurl + a["href"]).read())
                f.close()
                
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "tameside.xml"
    
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        
        print id
        html = open(os.path.join(cachedir, file)).read()
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        activity = activities.create_activity()
        activity.add_field("ActivitySourceID", id)
        
        celltext = page.find(attrs={"class":"celltext"})
        
        fields = {}
        desc = ""
        for p in celltext.findAll('p'):
            label = p.find("strong")
            try:
                if p.contents[1].__class__.__name__ == "NavigableString":
                    fields[label.contents[0]] = unicode(p.contents[1]).replace(u"\xa0","")
                else:
                    fields[label.contents[0]] = " ".join(p.contents[1].findAll(text=True)).replace(u"\xa0","")
            except IndexError:
                desc += "\n".join(p.findAll(text=True))
            except AttributeError:
                desc += "\n".join(p.findAll(text=True))
        
        activity.add_field("Description", desc)
        try: activity.add_field("Cost", fields["Cost:"])
        except KeyError: pass
        
        try: print fields["Open:"]
        except KeyError: pass
        
        if "Age Range:" in fields:
            m = re.search("([0-9]+) *- *([0-9]+)", fields["Age Range:"])
            if m:
                activity.add_field("MinAge", m.group(1))
                activity.add_field("MaxAge", m.group(2))
        
        try:
            dls = celltext.findAll("dl", attrs={"class": "inline"})
            strongs = dls[0].findAll("strong")
            print strongs
            dates = strongs[0].contents[0].split("-")
            dates = map(dateparse, dates)
            addr = dls[0].find("address")
            name = addr.contents[0].strip()
            activity.venue.add_field("ProviderVenueID", activity.hash(name))
            activity.venue.add_field("Name", name)
            activity.venue.add_field("Postcode", addr.contents[len(addr.contents)-1].strip())
            try:
                data = deflist_to_dict(dls[1])
                activity.add_field("ContactNumber", data["Telephone:"])
            except IndexError: pass
            except KeyError: pass
        except AttributeError:
            pass
        except IndexError:
            pass
        
        #activity.finish()
        #print activities.doc.toprettyxml(indent="    ")
        