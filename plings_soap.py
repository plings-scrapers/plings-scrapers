#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import plings_base
import pickle
import sys
from suds.client import Client

try:
    file = open(sys.argv[0]+".soap_pickle", "r")
    inputted_ids = pickle.load(file)
except:
    inputted_ids = {}

class Activities:
    client = None
    
    def register(self, key): 
        self.client = Client("http://feeds.plings.net/services/activities.php?wsdl", cache=None)#, doctor=doctor)
        r =  self.client.service.register(key)
        if r == 0:
            raise Exception("Registration with the API failed, check your API key")
        else:
            return r
        
    def create_activity(self):
        return Activity(self.client)
        
    def finish(self):
        pass

class Thing(plings_base.Thing):
    data = {}
    client = None
    
    def __init__(self, client):
        self.client = client
    
    def add_field(self, name, contents):
        if name != "ActivitySourceID" and contents != "":
            self.data[name] = contents
        elif name == "ActivitySourceID":
            self.id = contents
        
    def finish(self):
        for field in self.required_fields:
            if not field in self.data and not field == "ActivitySourceID":
                self.try_placeholder(field)

class ListThing():
    def __init__(self):
        self.data = []
    def add_field(self, ignored, contents):
        self.data.append(contents)

class DictThing():
    def __init__(self):
        self.data = {}
    def add_field(self, key, contents):
        self.data[key] = contents

class Activity(plings_base.Activity, Thing):
    extra = None
    
    def  __init__(self, client):
        self.categories = ListThing()
        self.keywords = ListThing()
        self.taxonomy = DictThing()
        Thing.__init__(self, client)
    
    def finish(self):
        Thing.finish(self)
        if self.client:
            if self.id  in inputted_ids:
                id = inputted_ids[self.id]
                print "Skipped"
            else:
                id = self.client.service.publishSimpleActivity(self.data)
                try: inputted_ids[self.id] = int(id)
                except ValueError: pass
                for cat in self.categories.data:
                    print self.client.service.addActivityCategory(id, cat)
                for key in self.keywords.data:
                    print self.client.service.addActivityKeyword(id, key)
                for taxonomy, category in self.taxonomy.data.items():
                    print self.client.service.addActivityCategoryOfTaxonomy(id, category, taxonomy)
                    pass
                print self.data
                print id
            file = open(sys.argv[0]+".soap_pickle", "w")
            pickle.dump(inputted_ids, file)
            return id
        else:
            return -1