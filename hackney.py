#!/usr/bin/env python
# Copyright (c) 2010, 2011 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details
import sys, os
from plings_sax import Activities
import BeautifulSoup, urllib2, mechanize
import re, datetime, dateutil.parser
import pickle

import json
def latlng2postcode(latlng):
    try:
        f = open("postcodes.pickle", "r")
        postcodes = pickle.load(f)
        f.close()
    except IOError:
        postcodes = {}
    if not latlng in postcodes:
        postcodes[latlng] = re.sub(" +", " ", json.loads(urllib2.urlopen("http://www.uk-postcodes.com/latlng/"+latlng+".json").read())["postcode"])
        f = open("postcodes.pickle", "w")
        pickle.dump(postcodes, f)
        f.close()
    return postcodes[latlng]    

cachedir = "cache/hackney/"
baseurl = "http://www.younghackney.org"

br = mechanize.Browser()

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    for i in range(1,52):
        print i
        br.open(baseurl+"/whatson/all.php?p="+str(i))
        html = br.response().read()
        page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        for eventitem in page.findAll("div", "eventitem"):
            for a in eventitem.findAll("a"):
                url = a["href"]
                m = re.search("/([0-9]+).php\?date=(.*)", url)
                id = m.group(1)+"-"+m.group(2)
                if not id.encode("utf8")+".html" in os.listdir(cachedir):
                    print id
                    html = urllib2.urlopen(baseurl+url.encode("utf8")).read()
                    f = open(os.path.join(cachedir, id.encode("utf8")+".html"), "w")
                    f.write(html)
                    f.close()
                else: print ".",
        print
    
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "hackney.xml"
    
    for file in os.listdir(cachedir):
        file = file.decode("utf8")
        if file.endswith(".html"):
            id = file[:-len(".html")]
        else: continue
        
        print id
        html = open(os.path.join(cachedir, file)).read()
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        
        activity = activities.create_activity()
        activity.add_field("ActivitySourceID", id)
        
        activity.add_field("Name", page.find("title").contents[0])
        
        """try:
            desc = re.search("<p><p>(.*)</p></p>", html).group(1)
        except AttributeError: desc = """""
        
        contents = page.find("div", "content").find("div", "text").contents
        desc = ""
        
        inDesc = False
        for content in contents:
            if inDesc: desc += unicode(content)
            if unicode(content) == '<div class="clear"></div>': inDesc = True
        
        m = re.search("([0-9]{1,2})((:|,|\.)([0-9]{2}))?([AaPp][Mm])? *(-|to) *([0-9]{1,2})((:|,|\.)([0-9]{2})([AaPp][Mm])?|([AaPp][Mm]))", desc)
        if m:
            def clevertime(hour, minute, apm):
                hour = int(hour)
                minute = int(minute)
                print hour,minute,apm
                if apm:
                    apm = apm.lower()
                    if apm == "pm" and hour < 12:
                        hour+=12
                elif hour < 8:
                    hour += 12
                return datetime.time(hour, minute)
            starttime = clevertime(m.group(1), m.group(4) or 0, m.group(5))
            endtime = clevertime(m.group(7), m.group(10) or 0, m.group(11) or m.group(12))
            date = dateutil.parser.parse(id.split("-",1)[1]).date()
            activity.add_field("Starts", datetime.datetime.combine(date, starttime).isoformat())
            activity.add_field("Ends", datetime.datetime.combine(date, endtime).isoformat())
        else:
            continue
        
        activity.add_field("Description",  desc +
            "<p><a href=\"http://www.younghackney.org/whatson/youth/"+id.split("-",1)[0]+".php?date="+id.split("-",1)[1]+"\">See this event on Youth Hackney</a></p>"
        )
        
        c=False
        for strong in page.find("div", "middlecol").findAll("strong"):
            field = strong.contents[0]
            value = strong.next.next
            if field == "Category: ":
                activity.categories.add_field("Category", value)
            elif field == "Venue: ":
                try:
                    bnn = value.split(",")[0]
                    activity.venue.add_field("ProviderVenueID", activity.hash(bnn))
                    activity.venue.add_field("Name", bnn)
                    m = re.match("[0-9\-]+", bnn)
                    if m: bnn = m.group(0)
                    activity.venue.add_field("BuildingNameNo", bnn)
                    try: postcode = re.search("[A-Z0-9]{2,4} [A-Z0-9]{3}", value).group(0)
                    except AttributeError:
                        id = id.split("-")[0]
                        if id.encode("utf8")+".js" in os.listdir(os.path.join(cachedir,"gmap")):
                            html = open(os.path.join(cachedir, "gmap", id.encode("utf8")+".js"), "r").read()
                        else:
                            html = urllib2.urlopen("http://www.younghackney.org/include/googlemap-event.js.php?ID="+id.encode("utf8")).read()
                            f = open(os.path.join(cachedir, "gmap", id.encode("utf8")+".js"), "w")
                            f.write(html)
                            f.close()
                        m = re.search("google.maps.LatLng\((.*?)\)", html)
                        latlng = m.group(1).replace(" ", "")
                        activity.venue.add_field("GeoCoordSystem", "WGS84DD")
                        activity.venue.add_field("GeoCoordX", latlng.split(",")[0])
                        activity.venue.add_field("GeoCoordY", latlng.split(",")[1])
                        activity.venue.add_field("Postcode", latlng2postcode(latlng))
                        postcode = latlng2postcode(latlng)
                    activity.venue.add_field("Postcode", postcode)
                except AttributeError:
                    c=True
                    continue
        if c: continue
        
        activity.finish()
        
    activities.finish()