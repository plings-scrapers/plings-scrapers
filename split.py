#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from xml.sax.saxutils import XMLGenerator

class PlingsSplitHandler(ContentHandler):
    i=0
    g=None
    filenum=0
    outname="output"
    
    def newfile(self):
        if self.g:
            self.g.endElement("Activities")
            self.g.endDocument()
        self.filenum += 1
        f = open(self.outname+"_"+str(self.filenum)+".xml", "w")
        f.write('<?xml version="1.0" encoding="UTF-8"?>')
        self.g = XMLGenerator(f, 'utf-8')
        if self.i:
            self.g.startElement("Activities", {})
    
    def __init__(self, outname, maxnum):
        self.outname = outname
        self.maxnum = maxnum
        self.newfile()
    
    def startElement(self, name, attrs):
        self.g.startElement(name, attrs)
    
    def characters(self, ch):
        self.g.characters(ch)
    
    def endElement(self, name):
        self.g.endElement(name)
        if name.lower() == "activity":
            self.i += 1
            if self.i % self.maxnum == 0:
                self.newfile()
    
def split(file, maxnum=5000):
    try:
        f = open(file, "r")
    except IOError:
        print "File "+file+" does not exist!"
        sys.exit(1)
    
    if file.endswith(".xml"):
        outname = file[:-len(".xml")]
    else:
        outname = file
    
    h = PlingsSplitHandler(outname, maxnum)
    parser = make_parser()
    parser.setContentHandler(h)
    parser.parse(f)
    
    return h.filenum

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: python split.py file.xml [activities_per_file]"
    
    else:
        if len(sys.argv) > 2:
            split(sys.argv[1], int(sys.argv[2]))
        else:
            split(sys.argv[1])

