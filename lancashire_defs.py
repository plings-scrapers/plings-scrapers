#!/usr/bin/env python
# Copyright (c) 2010 David Carpenter <caprenter@gmail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
 
def contact_details(page):
  data = {}
  for div in page.findAll("fieldset", { "class" : "fieldgroup group-act-contact" }):
     name = div.find("div", "field field-type-text field-field-act-contact-name")
     phone = div.find("div", "field field-type-text field-field-act-contact-phone")
     email = div.find("div", "field field-type-email field-field-act-email")
     #print email
     if name:
        l = name.find("div", "field-label")
        label = l.contents[0].strip()
        #print label
        #for div2 in div.find("div", {"class" : "field-items"}):
        l2 = name.find("div", "field-item odd")
        contact_name = l2.contents[0].strip()
        #print contact_name
        data [label] = contact_name
     if email:
        l = email.find("div", "field-label")
        label = l.contents[0].strip()
        #print label
        #for div2 in div.find("div", {"class" : "field-items"}):
        l2 = email.find("div", "field-item odd")
        #print l2
        email_address = l2.find("a").contents[0].strip()
        #print email_address
        data [label] = email_address
     if phone:
        #print phone
        l = phone.find("div", "field-label")
        #print l
        label = l.contents[0].strip()
        #print label
        #for div2 in div.find("div", {"class" : "field-items"}):
        l2 = phone.find("div", "field-item odd")
        phone_no1 = l2.contents[0].strip()
        l3 = phone.find("div", "field-item even")
        #print l3
        if l3:
          phone_no2 = l3.contents[0].strip()
          phone_no1 = phone_no1 + " / " + phone_no2
        data [label] = phone_no1
  return data
 
 
 
def ages(page):
  age_range = {}
  minage = page.find("div", "field field-type-number-integer field-field-act-from-age")
  #print minage
  minage_label = minage.find("div","field-label-inline-first").contents[0].strip()
  minage_value = minage.find("div","field-item odd")
  minage_value = minage_value.div.nextSibling.extract().strip() 
  age_range [minage_label] = minage_value
 
  maxage = page.find("div", "field field-type-number-integer field-field-act-to-age")
  #print maxage
  maxage_label = maxage.find("div","field-label-inline-first").contents[0].strip()
  maxage_value = maxage.find("div","field-item odd")
  maxage_value = maxage_value.div.nextSibling.extract().strip() 
  age_range [maxage_label] = maxage_value 
  return age_range
 
def description(page):
  desc = {}
  description_html = page.find("div", "field field-type-text field-field-act-desc")
  html = description_html.find("div","field-item odd").findAll(text=True) #findAll(text=True) removes html tags from the string
  desc["description"] = "".join(html) #html is a list of all text pieces, need to concatenate them
  #print html
  return desc
 
 
 
def venue_details(page):
  data = {}
  phone =  page.find("div","field field-type-text field-field-telephone")
  main_contact =  page.find("div","field field-type-text field-field-main-contact")
  if main_contact:
        contact_name = main_contact.find("div","field-item odd").contents[0].strip()
        l = main_contact.find("div", "field-label")
        label = l.contents[0].strip()
        data [label] = contact_name
  if phone:
        #print phone
        l = phone.find("div", "field-label")
        #print l
        label = l.contents[0].strip()
        #print label
        #for div2 in div.find("div", {"class" : "field-items"}):
        l2 = phone.find("div", "field-item odd")
        phone_no1 = l2.contents[0].strip()
        l3 = phone.find("div", "field-item even")
        #print l3
        if l3:
          phone_no2 = l3.contents[0].strip()
          phone_no1 = phone_no1 + " / " + phone_no2
        data [label] = phone_no1
  return data