#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details
import sys, os
from plings import Activities
import BeautifulSoup, urllib2, mechanize
import re, datetime, dateutil.parser

cachedir = "cache/skyride/"
baseurl = "http://www.goskyride.com"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    br = mechanize.Browser()
    html = br.open(baseurl + "/find-a-ride/").read()
    br.select_form(nr=0)
    select = br.find_control("ctl00$cphMain$ride1$ddlCity")
    cityids = map(lambda x: x.name, select.get_items())
    cityids.pop(0)
    for cityid in cityids:
        print cityid
        html = br.open(baseurl + "/find-a-ride/default.aspx?cityid="+cityid).read()
        while True:
            page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            for a in page1.findAll("a", id=re.compile(".*lnkMore")):
                print a["href"]
                m = re.search("eventid=([0-9]+)", a["href"])
                id = m.group(1)
                url2 = baseurl + "/find-a-ride/details.aspx?eventid=" + id
                if not (id+".html").encode("utf8") in os.listdir(cachedir):
                    html = urllib2.urlopen(url2.encode("utf8")).read()
                    print id
                    f = open(os.path.join(cachedir, id.encode("utf8")+".html"), "w")
                    f.write(html)
                    f.close()
            
            br.select_form(nr=0)
            try:
                if int(br.form["ctl00$cphMain$results1$paging1$hfCurrentPage"]) > int(br.form["ctl00$cphMain$results1$paging1$hfTotalRecs"])/int(br.form["ctl00$cphMain$results1$paging1$hfRecsPerPage"]):
                    break
            except mechanize._form.ControlNotFoundError:
                break
            
            br.form.new_control("text", "__EVENTTARGET", {})
            br.form["__EVENTTARGET"] = "ctl00$cphMain$results1$paging1$btnNext1"
            br.submit()
            html = br.response().read()
        
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "skyride.xml"
    
    for file in os.listdir(cachedir):
        try:
            file = file.decode("utf8")
            if file.endswith(".html"):
                id = file[:-len(".html")]
            
            print id
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            activity.add_field("Name", "Skyride - "+page.find(id="ctl00_cphMain_details1_lblRideEventTitle").contents[0])
            
            details = {}
            for tag in ["StartTime", "AgeRestrictions", "DurationTotal", "Pace", "Distance", "SuitableRoadBikes", "SuitableChildTrailers"]:
                details[tag] = " ".join( page.find(id="ctl00_cphMain_details1_lbl"+tag) .findAll(text=True))
            print details
            starts = dateutil.parser.parse(details["StartTime"])
            m = re.match("([0-9]+) hrs?( ([0-9]+) mins?)?", details["DurationTotal"])
            duration = datetime.timedelta(hours=int(m.group(1)), minutes=int(m.group(3) or "0"))
            activity.add_field("Starts", starts.isoformat())
            activity.add_field("Ends", (starts+duration).isoformat())
            
            desc = "<p>"+page.find(id="ctl00_cphMain_details1_lblDescription").contents[0]+"</p>"
            for tag in ["AgeRestrictions", "Pace", "Distance", "SuitableRoadBikes", "SuitableChildTrailers"]:
                desc += "<p>"+tag+": "+details[tag]+"</p>"
            desc += "<p><a href=\"http://www.goskyride.com/find-a-ride/details.aspx?eventid="+id+"\">See this event on the skyride website.</a></p>"
            activity.add_field("Description", desc)
            
            vname = page.find(id="ctl00_cphMain_details1_lblAddress").contents[0]
            activity.venue.add_field("ProviderVenueID", activity.hash(vname))
            activity.venue.add_field("Name", vname)
            pc = page.find(id="ctl00_cphMain_details1_lblAddress").contents[-2].strip(".")
            activity.venue.add_field("Postcode", pc)
            
            activity.keywords.add_field("Keyword", "cycling")
            
            activity.finish()
        
        except AttributeError: print "ERROR"
        
    activities.finish()