#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
from xml.dom.minidom import Document
import plings_base
import copy
import datetime

day2int = plings_base.day2int
startdate = plings_base.startdate
enddate = plings_base.enddate

class Activities:
    def __init__(self):
        self.doc = Document()
        self.activities = self.doc.createElement("Activities")
        self.doc.appendChild(self.activities)
        
    def create_activity(self):
        activity = Activity(self.doc, self.doc.createElement("Activity"))
        activity.parent = self
        return activity
    
    def finish(self):
        out = open(self.outname, "w")
        self.doc.writexml(out, encoding="utf-8")
        
    def cloneactivity(self, activity, newid):
        activity2 = copy.deepcopy(activity)
        activity2.parent = self
        activity2.update_field("ActivitySourceID", newid)
        return activity2
        
class Thing(plings_base.Thing):
    xml_element = None
    doc = None
    id = "Unknown"
    name = "Unknown"
    
    def add_field(self, name, contents):
        text = contents.strip()
        if text != "":
            if name == "Name": self.name = text
            elif name == "ActivitySourceID": self.id = text
            elif name == "ProviderVenueID": self.id = text
            elif name == "DPProviderID": self.id = text
            el = self.doc.createElement(name)
            txt = self.doc.createTextNode(text.encode("utf-8"))
            el.appendChild(txt)
            self.xml_element.appendChild(el)
        
    def update_field(self, name, contents):
        self.xml_element.removeChild(self.xml_element.getElementsByTagName(name)[0])
        self.add_field(name, contents)
    
    def finish(self):
        for field in self.required_fields:
            if len(self.xml_element.getElementsByTagName(field)) == 0:
                self.try_placeholder(field)

    def __init__(self, pdoc, element):
        self.doc = pdoc
        self.xml_element = element

class Venue(plings_base.Venue, Thing):
    pass

class Organisation(plings_base.Organisation, Thing):
    pass

class Taxonomy():
    cats = None
    
    def __init__(self, cats):
        self.cats = cats

    def add_field(self, tax, text):
        el = self.cats.doc.createElement("Category")
        el.attributes["tax"] = tax 
        txt = self.cats.doc.createTextNode(text)
        el.appendChild(txt)
        self.cats.xml_element.appendChild(el)

class Activity(plings_base.Activity, Thing):   
    def __init__(self, pdoc, element):
        Thing.__init__(self, pdoc, element)
        if not self.disable_venue:
            self.venue = Venue(self.doc, self.doc.createElement("Venue"))
            self.xml_element.appendChild(self.venue.xml_element)
        self.keywords = Thing(self.doc, self.doc.createElement("Keywords"))
        self.xml_element.appendChild(self.keywords.xml_element)
        self.categories = Thing(self.doc, self.doc.createElement("Categories"))
        self.xml_element.appendChild(self.categories.xml_element)
        self.taxonomy = Taxonomy(self.categories)
        
    def add_org(self):
        self.org = Organisation(self.doc, self.doc.createElement("ActivityProvider"))
        self.xml_element.appendChild(self.org.xml_element)
    
    def __deepcopy__(self, memo):
        activity2 = copy.copy(self)
        activity2.recur_time = copy.copy(self.recur_time)
        activity2.recur_day = copy.copy(self.recur_day)
        activity2.recur_currdate = copy.copy(self.recur_currdate)
        
        activity2.xml_element = self.xml_element.cloneNode(True)
        n = activity2.venue.name
        id = activity2.venue.id
        activity2.venue = Venue(self.doc, activity2.xml_element.getElementsByTagName("Venue")[0])
        activity2.venue.name = n
        activity2.venue.id = id
        if self.org:
            n = activity2.org.name
            id = activity2.org.id
            activity2.org = Organisation(self.doc, activity2.xml_element.getElementsByTagName("ActivityProvider")[0])
            activity2.org.name = n
            activity2.org.id = id
        return activity2
    
    def finish(self):
        if not self.disable_venue:
            self.venue.finish()
        if self.org != None:
            self.org.finish()
        Thing.finish(self)
        self.parent.activities.appendChild(self.xml_element)
    
    def recur(self):
        #print self.recur_currdate
        self.recur_currdate += datetime.timedelta(days=7)
        while self.recur_currdate  <= self.enddate:
            #print self.recur_currdate
            self.recur_count += 1
            activity2 = copy.deepcopy(self)
            self.parent.activities.appendChild(activity2.xml_element)
            activity2.update_field("ActivitySourceID", self.seriesid+"-"+self.recur_currdate.strftime("%W%Y"))
            activity2.update_field("Starts", datetime.datetime.combine(self.recur_currdate, self.recur_time[0]).strftime("%Y-%m-%dT%H:%M:%S"))
            activity2.update_field("Ends", datetime.datetime.combine(self.recur_currdate, self.recur_time[1]).strftime("%Y-%m-%dT%H:%M:%S"))
            activity2.add_field("LinkWithActivitySourceID", self.id)
            self.recur_currdate += datetime.timedelta(days=7)

