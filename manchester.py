#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings import Activities
import datetime
import dateutil.parser
import re
import sys
import os
import json, pickle

class NullException(Exception):
    pass

def latlng2postcode(latlng):
    try:
        f = open("postcodes.pickle", "r")
        postcodes = pickle.load(f)
        f.close()
    except IOError:
        postcodes = {}
    if not latlng in postcodes:
        postcodes[latlng] = re.sub(" +", " ", json.loads(urllib2.urlopen("http://www.uk-postcodes.com/latlng/"+latlng+".json").read())["postcode"])
        f = open("postcodes.pickle", "w")
        pickle.dump(postcodes, f)
        f.close()
    return postcodes[latlng]

baseurl = "http://www.manchester.gov.uk"
cachedir = "cache/manchester/"

if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    for i in range(ord("a"), ord("z")+1):
        try:
            url = baseurl+"/directory/30/a_to_z/"+chr(i)
            print url
            html = urllib2.urlopen(url).read()
            page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            for li in page1.find(attrs={"class":"aToZList"}).findAll("li"):
                try:
                    link = li.a["href"]
                    print link
                    m = re.search("/([0-9]+)/", link)
                    id = m.group(1)        
                    if not id+".html" in os.listdir(cachedir):
                        print id
                        f = open(cachedir+id+".html", "w")
                        f.write(urllib2.urlopen(baseurl+link).read())
                        f.close()
                except TypeError: continue
        except AttributeError: continue

elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "manchester.xml"
    
    for file in os.listdir(cachedir):
        if file.endswith(".html"):
            id = file[:-len(".html")]
        try:
            #print id, 
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity = activities.create_activity()
            activity.add_field("ActivitySourceID", id)
            desc = ""
            days = ""
            times = [None, None]
            for tr in page.find("div", "directory").find("ul").findAll("li"):
                k = unicode(tr.h3.contents[0]).strip()
                v = unicode(tr.div.contents[0]).strip()
                
                if k == "Activity": activity.add_field("Name", v)
                elif k == "Min age": activity.add_field("MinAge", v)
                elif k == "Min age": activity.add_field("MaxAge", v)
                elif k == "Min age": activity.add_field("MaxAge", v)
                elif k == "Cost": activity.add_field("Cost", v)
                elif k == "Contact name": activity.add_field("ContactName", v)
                elif k == "Contact number": activity.add_field("ContactNumber", v)
                elif k == "Additional information or description of the event": desc += v
                elif k == "Registration needed": desc += v
                elif k == "Day and Date":days = v
                elif k == "Start time": times[0] = dateutil.parser.parse(v).time()
                elif k == "Finish time": times[1] = dateutil.parser.parse(v).time()
                elif k == "Venue":
                    activity.venue.add_field("ProviderVenueID", activity.hash(v))
                    activity.venue.add_field("Name", v)
                elif k == "Address":
                    #print v
                    pass
                elif k == "Location":
                    latlng = tr.div.find("input", id="map_marker_location_348")["value"]
                    activity.venue.add_field("GeoCoordSystem", "WGS84DD")
                    activity.venue.add_field("GeoCoordX", latlng.split(",")[0])
                    activity.venue.add_field("GeoCoordY", latlng.split(",")[1])
                    activity.venue.add_field("Postcode", latlng2postcode(latlng))
            activity.add_field("Description", desc)
            
            m = re.search("[0-9]+", days)
            if m == None:
                for day in days.split(" "):
                    try:
                        activity2 = activities.cloneactivity(activity, id+"-"+day)
                        activity2.setup_recurring(day, times)
                        activity2.finish()
                        activity2.recur()
                    except Exception,e: print e
            else:
                dates = re.findall("[0-9]{2}/[0-9]{2}/[0-9]{2}", days)
                for date in dates:
                    try:
                        activity2 = activities.cloneactivity(activity, id+"-"+day)
                        d = dateutil.parser.parse(date).date()
                        activity2.add_field("Starts", datetime.datetime.combine(d,times[0]).isoformat())
                        activity2.add_field("Ends", datetime.datetime.combine(d,times[1]).isoformat())
                        activity2.finish()
                    except Exception,e: print e
            #print
            
        except Exception,e: print e 
    
    activities.finish()
    #print activities.doc.toprettyxml(indent="    ")