#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import config
import sys
import urllib2

def post(file, laname, ext="-L"):
    key = config.keys[laname]+ext
    
    req = urllib2.Request("http://feeds.plings.net/services/plings-in.php?APIKey="+key,
            headers = { "Content-Type": "text/xml", },
            data = open(file).read())
    print urllib2.urlopen(req).read()

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: python xml_post.py file.xml laname [live]"
        sys.exit(0)
    
    ext = "-D"
    if len(sys.argv) > 3:
        if sys.argv[3] == "live":
            ext = "-L"     
    post(sys.argv[1], sys.argv[2], ext)


"""
if (key_exists($locala, $keys)) $key = $keys[$locala].$append;
else $key = $keys[preg_replace('/[0-9]/', '', $locala)].$append;
$xml = file_get_contents("out/$locala.xml");
print sendXmlOverPost("http://feeds.plings.net/services/plings-in.php?APIKey=".$key, $xml);
?>
"""
