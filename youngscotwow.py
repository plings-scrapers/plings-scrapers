#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import BeautifulSoup
import urllib2
from plings_sax import Activities
import datetime
import dateutil.parser
import re
import sys
import os

cachedir = "cache/youngscotwow/"
baseurl = "http://www.youngscotwow.org"
 
 
if len(sys.argv) < 2:
    print "You must supply an action: get or scrape"

elif sys.argv[1] == "get":
    for i in range(1,250):
        url = baseurl + "/search/results/?Category=&Where=&What=&page=" + str(i)
        html = urllib2.urlopen(url).read() 
        page1 = BeautifulSoup.BeautifulSoup(html, convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
 
        for act in page1.findAll(attrs={"class": "listeritemdetail"}):
            a = act.find("a")
            url2 = baseurl + a["href"]
            print url2
            id = re.sub("(opportunity|\/)", "", a["href"])
            if not (id+".html").encode("utf8") in os.listdir(cachedir):
                html = urllib2.urlopen(url2.encode("utf8")).read()
                print id
                f = open(os.path.join(cachedir, id.encode("utf8")+".html"), "w")
                f.write(html)
                f.close()
 
elif sys.argv[1] == "scrape":
    activities = Activities()
    activities.outname = "youngscotwow.xml"
 
    for file in os.listdir(cachedir):
        try:
            file = file.decode("utf8")
            if file.endswith(".html"):
                id = file[:-len(".html")]
            print id
 
            
            ##Create our activity
            activity = activities.create_activity()
            ##Write the ActivitySourceID
            activity.add_field("ActivitySourceID", id)
            
            ##Open the saved HTML for parsing
            html = open(os.path.join(cachedir, file)).read()
            page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
            
            activity.add_field("Name", page.find("h1").contents[0])
            
            activity.add_field("Description",
                unicode(page.find("div", "articlecontentwrapper")).split("<h2>Opportunity Details</h2>")[0] \
                + "<p><a href=\"http://www.youngscotwow.org/opportunity/"+id+"/\">View this event on YoungScotWow</a></p>" )
 
            m = re.findall("[A-Z]{1,2}[0-9]{2} [0-9][A-Z]{2}", html)
            if len(m) < 2:
                continue
            activity.venue.add_field("Postcode", m[0])
            
            sections = page.findAll("div", "sectionwrapper")
            for section in sections:
                try:
                    if section.img:
                        label = section.img["alt"]
                        value = section.contents[1]
                    else:
                        label = section.label.contents[0]
                        value = section.contents[3].contents[0]
                    if label != "Email": value = unicode(value).strip()
                    if label == "Opportunity Organiser":
                        activity.venue.add_field("ProviderVenueID", activity.hash(value))
                        activity.venue.add_field("Name", value)
                    elif label == "Telephone":
                        activity.add_field("ContactNumber", value)
                    elif label == "Email":
                        activity.add_field("ContactEmail", value.contents[0])
                    elif label == "Main Contact":
                        activity.add_field("ContactName", value)
                    elif label == "Booking":
                        pass
                    elif label == "Organiser's website":
                        pass
                    elif label == "Useful links":
                        pass
                    elif label == "Disability Facilities" or label == "Ethnic Facilities" or label == "Wheelchair Access" or label == "Disabled Access":
                        pass
                    
                    elif label == "Event Date":
                        print value.split("@")
                        d = dateutil.parser.parse(value)
                        activity.add_field("Starts", d.isoformat())
                        activity.add_field("Ends", (d+datetime.timedelta(hours=2)).isoformat())
                    elif label == "Event Cost":
                        if value != "Varies":
                            activity.add_field("Cost", value)
                    elif label == "View on Map":
                        pass
                    else:
                        print label, unicode(value).strip()
                except IndexError: pass
                except AttributeError: pass
            
            print
            activity.finish()
        except KeyError, e: print e
        except AttributeError, e: print e
        except ValueError, e: print e
        except IndexError, e: print e
        except urllib2.HTTPError: pass
        except urllib2.URLError:
            from time import sleep
            sleep(10)
        except IOError:
            print "IOError"
 
    activities.finish()
    #print activities.doc.toprettyxml(indent="    ")
        