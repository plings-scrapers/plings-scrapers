import http_scraper
import re

class Scraper(http_scraper.Scraper):
    def get(self):
        f = open("bookmarks.html", "w")
        f.write("""<!DOCTYPE NETSCAPE-Bookmark-file-1>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<TITLE>Bookmarks</TITLE>
<H1>Bookmarks</H1>
<DL><p>\n""")
        for i in range(1,7):
            url = "http://www.raf.mod.uk/aircadets/findasquadron/?/showpage/9/page/"+str(i)
            html = self.http_cache(url, self.searchre.match(url).group(1), "list", returnhtml=True)
            vids = re.findall("\"http://www.raf.mod.uk/aircadets/findasquadron/playVideo/\?id=([^\"]*)\" title=\"([^\"]*)\"", html)
            for vid, title in vids:
                url = "http://www.youtube.com/watch?v="+vid
                print url, title
                f.write("<DT><A HREF=\""+url+"\">"+title+"</A>\n")
        f.write("</DL></p>\n")
        f.close()