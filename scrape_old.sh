if [ -z $1 ]
	then echo "Please specify a scraper name"
else
	cd ~/plings-scrapers
	rm cache/$1/*
	rm cache/$1/*/*
        rm $1*.xml
	python $1.py get
	python $1.py scrape
	python split.py $1.xml
	for f in $1_*.xml
		do python xml_post.py $f $1 live
	done
fi
