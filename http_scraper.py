import sys, os, urllib2
import re, datetime
import BeautifulSoup
from plings_base import day2int
import dateutil.parser

class Scraper:
    baseurl = ""
    cachedir = None
    idre = re.compile(".*?([0-9]+)$")
    searchre = re.compile(".*?([0-9]+)$")
    name = None

    def http_cache(self, url, id, subfolder=None, returnhtml=True ,ext=".html"):
        if url == None or id == None: return
        if self.cachedir == None: cachedir = os.path.join("cache", self.name)
        else: cachedir = self.cachedir
        if subfolder != None: cachedir = os.path.join(cachedir, subfolder)
        
        if not os.path.exists(cachedir):
            os.makedirs(cachedir)
        
        id = id.encode("utf8")
        url = url.encode("utf8")
        if not id+ext in os.listdir(cachedir):
            print id
            html = urllib2.urlopen(self.baseurl+url).read()
            f = open(os.path.join(cachedir, id+ext), "w")
            f.write(html)
            f.close()
            if returnhtml:
                return html
        elif returnhtml:
            return open(os.path.join(cachedir, id+ext), "r").read()
        else: print "."

    def get(self):
        for url in self.url_list:
            print url
            self.http_cache(url, self.idre.match(url).group(1), "activity")

    def get_page(self, url, type="list", idre=None, id=None):
        if not idre:
            idre = self.searchre
        if not id:
            id = idre.match(url).group(1)
        html = self.http_cache(url, id, type, returnhtml=True)
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        return (id, page)

    def scrape(self):
        if hasattr(self, "Activities"):
            self.activities = self.Activities()
            self.activities.outname = self.name+".xml"
        
        if self.cachedir == None: cachedir = os.path.join("cache", self.name)
        else: cachedir = self.cachedir
        cachedir = os.path.join(cachedir, "activity")
        for file in os.listdir(cachedir):
            file = file.decode("utf8")
            if file.endswith(".html"):
                id = file[:-len(".html")]
            else: continue
            
            #print id
            html = open(os.path.join(cachedir, file)).read()
            self.scrape_html(id, html)
        
        if hasattr(self, "Activities"):
            self.activities.finish()
            
    def scrape_html(self, id, html):
        page = BeautifulSoup.BeautifulSoup(html, fromEncoding="utf-8", convertEntities=BeautifulSoup.BeautifulSoup.HTML_ENTITIES)
        self.scrape_page(id, html, page)
        
    def scrape_page(self, id, html, page):
        pass
    
    def bs_string(self, el):
        return "".join(map(unicode, el.contents)).strip()
    
    def bs_following(self, el, key, toString=True):
        try:
            key_el = el.find(text=key).parent
            while True:
                if key_el.nextSibling == None:
                    key_el = key_el.parent
                else: break
            if unicode(key_el.nextSibling).strip() == "": value_el = key_el.nextSibling.nextSibling
            else:
                value_el = key_el.nextSibling
            if toString:
                if key_el.nextSibling.__class__.__name__ == "NavigableString":
                    return unicode(value_el)
                else:
                    return self.bs_string(value_el)
            else:
                return value_el
        except AttributeError:
            return ""
        
    def extract_days(self, text):
        days = re.findall("(mon|tues|wednes|thurs|fri|satur|sun)day", text, re.IGNORECASE)
        return list(set(days))
    
    def extract_times(self, text):
        m = re.search("([0-9]{1,2})((:|,|\.)([0-9]{2}))?([AaPp][Mm])? *(-|to) *([0-9]{1,2})((:|,|\.)([0-9]{2})([AaPp][Mm])?|([AaPp][Mm]))", text)
        if m:
            def clevertime(hour, minute, apm):
                hour = int(hour)
                minute = int(minute)
                #print hour,minute,apm
                if apm:
                    apm = apm.lower()
                    if apm == "pm" and hour < 12:
                        hour+=12
                elif hour < 8:
                    hour += 12
                return datetime.time(hour, minute)
            try:
                starttime = clevertime(m.group(1), m.group(4) or 0, m.group(5))
                endtime = clevertime(m.group(7), m.group(10) or 0, m.group(11) or m.group(12))
            except ValueError:
                return False
            return (starttime, endtime)
        else:
            return False
        
    def autodate(self, activity, text):
        text = text.lower()
        days = []
        pieces = text.split("day")
        #print pieces
        for piece in pieces:
            if len(days):
                times = self.extract_times(piece)
                if times:
                    #print days, times
                    activity.multiple_recur(self.activities, days, times)
                    days = []
            m = re.search("(to|\-)?[ ]*(mon|tues|wednes|thurs|fri|satur|sun|week)$", piece)
            if m:
                if m.group(2) == "week":
                    for i in range(0,5):
                        days.append(i)
                elif m.group(1):
                    if len(days):
                        day = days[-1]
                        endday = day2int(m.group(2))
                        while day != endday:
                            day += 1
                            if (day == 7): day = 0
                            days.append(day)
                else:
                    days.append(day2int(m.group(2)))
                    
    def autoaddress(self, activity, text):
        bits = text.split(",")
        i=0
        dobreak = False
        for bit in bits:
            bit = bit.strip(" \r\n")
            m = re.search("(.*) ([A-Za-z]{1,2}[0-9]{1,2} [0-9][A-Za-z]{2})", bit)
            if m:
                activity.venue._Postcode = m.group(2)
                bit = m.group(1)
                dobreak = True
            if i==0:
                activity.venue._Name = bit
                m = re.match("([0-9\-]+) (.*)", bit)
                if m:
                    activity.venue._BuildingNameNo = m.group(1)
                    activity.venue._Street = m.group(2)
            elif i==1: self.autoaddress_street(activity, bit)
            elif i==2: activity.venue._Town = bit
            elif i==3: activity.venue._County = bit
            if dobreak: break
            i+=1
        return dobreak
    
    def autoaddress_street(self, activity, street):
        m = re.match("([0-9\-]+) (.*)", street)
        if m:
            activity.venue._BuildingNameNo = m.group(0)
            activity.venue._Street = m.group(1)
        else:
            activity.venue._Street = street
            
    def nice_date(self, text):
        return dateutil.parser.parse(text).isoformat()